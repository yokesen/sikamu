<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'users_cabinet';

    protected $fillable = ['email_verification','id_cms_privileges'];

    use HasFactory;
}
