<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Cookie;
use DB;

class ApiMySessionController extends Controller
{
    public function mySession(){
      if (Session::get('user')) {
        $user= DB::table('users_cabinet')->select('name','email','photo','uuid','email_verification','username','phone')->where('uuid',Session::get('user')->uuid)->first();
      }else{
        $user = [];
      }
      return json_encode($user);
    }
}
