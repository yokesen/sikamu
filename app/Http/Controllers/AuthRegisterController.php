<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Jenssegers\Agent\Agent;
use Cookie;
use Session;
use Illuminate\Support\Facades\Hash;

class AuthRegisterController extends Controller
{
    public function submitRegister(Request $request){

      $rules = [
          'g-recaptcha-response' => 'required|captcha',
          'name' => 'required|min:2|max:50',
          'email' => 'required|email|unique:users_cabinet',
          'phone' => 'required|min:10|unique:users_cabinet',
          'password' => 'required|string|min:8|max:20|confirmed'
      ];

      $messages = [
          'g-recaptcha-response.required' => 'Kesalahan : Kamu harus lulus uji Captcha ini',
          'g-recaptcha-response.captcha' => 'Kesalahan : Kamu harus lulus uji Captcha ini',
          'name.required' => 'Kesalahan : Nama wajib diisi!',
          'name.min' => 'Kesalahan : Nama depan minimal 2 huruf!',
          'email.required' => 'Kesalahan : Email wajib diisi!',
          'email.email' => 'Kesalahan : Email tidak valid!',
          'email.unique'  => 'Kesalahan : Email sudah terdaftar!',
          'phone.required' => 'Kesalahan : Nomor Whatsapp wajib diisi!',
          'phone.min' => 'Kesalahan : Nomor Whatsapp-mu sepertinya salah!',
          'password.required' => 'Kesalahan : Password-mu masih kosong nih!',
          'password.min' => 'Kesalahan : Password-mu minimal harus 8 huruf/angka!',
          'password.max' => 'Kesalahan : Password-mu terlalu panjang!',
          'password.confirmed' => 'Kesalahan : Password dan Password Konfirmasinya salah!',

      ];

      $validator = Validator::make($request->all(), $rules, $messages);

      if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $agent = new Agent();
      if($agent->isPhone()){
        $screen = "Phone";
      }elseif($agent->isTablet()){
        $screen = "Tablet";
      }elseif($agent->isDesktop()){
        $screen = "Desktop";
      }
      $platform = $agent->platform();
      $version_platform = $agent->version($platform);
      $device = $agent->device();
      $browser = $agent->browser();
      $version_browser = $agent->version($browser);
      $languages = serialize($agent->languages());
      $robot = $agent->robot();
      $ip = request()->ip();
      $uuid = sha1($request->email).time();

      $whatsapp = preg_replace("/[^0-9]/", "", $request->phone );

      $insert = DB::table('users_cabinet')->insertGetId([
        'name' => $request->name,
        'email' => $request->email,
        'email_validation' => 'new',
        'email_verification' => 'new',
        'password' => Hash::make($request->password),
        'phone' => $request->phone,
        'whatsapp' => $whatsapp,
        'providerId' => $uuid,
        'providerOrigin' => 'register page',
        'id_cms_privileges' => '7',
        'parent' => Cookie::get('ref'),
        'utm_source' => Cookie::get('utm_source'),
        'utm_medium' => Cookie::get('utm_medium'),
        'utm_campaign' => Cookie::get('utm_campaign'),
        'utm_term' => Cookie::get('utm_term'),
        'utm_content' => Cookie::get('utm_content'),
        'uuid' => $uuid,
        'ipaddress' => $ip,
        'screen' => $screen,
        'platform' => $platform,
        'platformVersion' => $version_platform,
        'device' => $device,
        'browser' => $browser,
        'browserVersion' => $version_browser,
        'language' => $languages,
        'robot' => $robot
      ]);

      $params = [
        'linkcode' => ENV('APP_URL').'/onboarding/mail/'.$request->email.'/verify/'.$uuid,
        'time' => date('d-m-Y H:i:s')
      ];

      $mail = DB::table('daftar_queue_email')->insert([
        'type' => 'transactional',
        'content' => 'register',
        'uuid' => $uuid,
        'params' => serialize($params)
      ]);

      $login = DB::table('users_cabinet')->where('email',$request->email)->first();
      $priv = DB::table("cms_privileges")->where("id", $login->id_cms_privileges)->first();
      $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', $login->id_cms_privileges)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();
      $menus = DB::table('cms_menus_privileges')->where('cms_menus_privileges.id_cms_privileges', $login->id_cms_privileges)->join('cms_menus', 'cms_menus.id', 'cms_menus_privileges.id_cms_menus')->get();

      Session::put('user', $login);
      Session::put('priv', $priv);
      Session::put('modul', $roles);
      Session::put('menu', $menus);

      return redirect()->route('viewDashboard');

    }
}
