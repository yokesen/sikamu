<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;
Use Alert;

class GettingStartedController extends Controller
{
      public function viewGettingStarted(){
        $menu = 'Getting Started';
        /*-----------------*/
        $user = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->first();
        if ($user->email_verification == "new") {
          return view('app.ims-verifikasi-email',compact('menu','user'));
        }

        return view('app.ims-action',compact('menu','user'));
      }

      public function editProfile(){
        $menu = 'edit profile';
        /*-----------------*/
        $user = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->first();
        return view('app.ims-profile',compact('menu','user'));
      }

      public function editBank(){
        $menu = 'edit Bank';
        /*-----------------*/
        $user = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->first();
        return view('app.ims-bank',compact('menu','user'));
      }

  // public function appDoOnboard(){
  //   $menu = 'Getting Started';
  //   /*-----------------*/
  //   $user = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->first();
  //   if ($user->email_verification == "new") {
  //
  //   }
  //   return view('app.ims-action',compact('menu','user'));
  // }

  // public function SubmitGettingStarted(Request $request){
  //   if (Session::get('user')) {
  //     $validator = Validator::make($request->all(), [
  //         'username' => 'required|min:3',
  //         'name' => 'required|min:3'
  //     ]);
  //
  //     if ($validator->fails()) {
  //         return back()->with('errors', $validator->messages()->all()[0])->withInput();
  //     }
  //
  //     $update = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->update([
  //       'username' => $request->username,
  //       'email' => $request->email,
  //       'phone' => $request->whatsapp,
  //       'name' => $request->name,
  //     ]);
  //
  //     $bank = DB::table('bank_lists')->where('id',$request->bank)->first();
  //
  //     $insert = DB::table('banks')->insert([
  //       'user_id' => Session::get('user')->id,
  //       'uuid' => Session::get('user')->uuid,
  //       'bank_name' => $request->bank,
  //       'account_number' => $request->rekening,
  //       'account_name' => $request->name
  //     ]);
  //     return $update;
  //   }
  //   return "Illegal Access";
  // }

  // public function sendVerifyEmail(Request $request){
  //   if (Session::get('user')) {
  //     $code1 = rand(0,9);
  //     $code2 = rand(0,9);
  //     $code3 = rand(0,9);
  //     $code4 = rand(0,9);
  //     $code = $code1.$code2.$code3.$code4;
  //
  //     $update = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->update([
  //       'email' => $request->email,
  //       'email_verification' => $code
  //     ]);
  //
  //     $params = ['code' => $code];
  //
  //     $insert = DB::table('daftar_queue_email')->insert([
  //       'user_id' => Session::get('user')->id,
  //       'params' => serialize($params)
  //     ]);
  //     return $update;
  //   }
  //   return "Illegal Access";
  // }

  // public function verifiyingEmail($uuid){
  //
  //
  //   if (Session::get('user')) {
  //     $user = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->first();
  //     if ($request->code == $user->email_verification) {
  //       $update = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->update([
  //         'email_verification' => 'verified'
  //       ]);
  //       return 'kode benar';
  //     }
  //
  //     return "kode salah";
  //   }
  //   return "Illegal Access";
  // }
}
