<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ApiBankListController extends Controller
{
    public function bankList(){
      $banks = DB::table('bank_lists')->get();
      return json_encode($banks);
    }
}
