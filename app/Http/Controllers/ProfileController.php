<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Image;
use Storage;
use Session;

class ProfileController extends Controller
{
    public function editProfilePost(Request $request){
      $uuid = uuid();
      if($request->file('photo'))
      {
        $image = $request->file('photo');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/

      }

      $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
          'username' => $request->username,
          'name' => $request->name,
          'photo' => env('IMG_USER').$thumb_name,
          'dob' => $request->dob
      ]);

      $user = profile();
      Session::put('user',$user);
      return redirect()->back();
    }

    public function uploadBukuTabungan(Request $request){
      $uuid = uuid();
      if($request->file('gambar'))
      {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }else{
          $image_normal = Image::make($image)->widen(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();

        Storage::disk('upcloud')->put('images/orbitrade/user/'.$image_name, $image_normal->__toString());

        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
          'photoTabungan' => $image_name
        ]);
      }

      return $image_name;
    }

    public function uploadKtp(Request $request){
      $uuid = uuid();
      if($request->file('gambar'))
      {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }else{
          $image_normal = Image::make($image)->widen(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();

        Storage::disk('upcloud')->put('images/orbitrade/user/'.$image_name, $image_normal->__toString());

        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
          'photoKTP' => $image_name
        ]);
      }

      return $image_name;
    }
}
