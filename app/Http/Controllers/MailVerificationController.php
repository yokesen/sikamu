<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Client;
use Alert;
use Session;

class MailVerificationController extends Controller
{
    public function askToVerifyMail(){
      $user = DB::table('users_cabinet')->where('email',Session::get('user')->email)->first();
      if(!empty($user)){
        $params = [
          'linkcode' => ENV('APP_URL').'/onboarding/mail/'.$user->email.'/verify/'.$user->uuid,
          'time' => date('d-m-Y H:i:s')
        ];

        $mail = DB::table('daftar_queue_email')->insert([
          'type' => 'transactional',
          'content' => 'register',
          'uuid' => $user->uuid,
          'params' => serialize($params)
        ]);
      }
      Alert::success('Hi Email Verifikasi sudah dikirimkan ke '.$user->email,'Kalau diinbox atau promotion ga ketemu, coba cari di spam/junk mail ya.')->showConfirmButton('Okay!', '#DB1430');
      return redirect()->back();
    }

    public function verifyActionMail($email,$code){
      if ($email == Session::get('user')->email) {
        $user = DB::table('users_cabinet')->where('email',$email)->first();
        if($user->uuid == $code){
          $update = DB::table('users_cabinet')->where('uuid',$user->uuid)->update([
            'email_verification' => 'verified'
          ]);

          $priv = DB::table("cms_privileges")->where("id", $user->id_cms_privileges)->first();
          $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', $user->id_cms_privileges)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();
          $menus = DB::table('cms_menus_privileges')->where('cms_menus_privileges.id_cms_privileges', $user->id_cms_privileges)->join('cms_menus', 'cms_menus.id', 'cms_menus_privileges.id_cms_menus')->get();

          Session::put('user', $user);
          Session::put('priv', $priv);
          Session::put('modul', $roles);
          Session::put('menu', $menus);

          Alert::success('Selamat Email Berhasil diverifikasi!','Sekarang lanjut isi data diri ya')->showConfirmButton('Okay!', '#DB1430');
          return redirect()->route('viewDashboard');
        }
      }

      Alert::warning('Wah email kamu kok ga bisa terverifikasi ya?','Coba periksa kembali email kamu atau klik dibawah ini untuk minta kode verifikasi baru')->showConfirmButton('Okay!', '#DB1430');
      return redirect()->route('viewGettingStarted');
    }
}
