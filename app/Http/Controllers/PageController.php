<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class PageController extends Controller
{
    public function viewHomepage(){
      $user = DB::table('users_cabinet')->where('id','4517')->first();
      $menu = 'homepage';
      /*-----------------*/

      return view('pages.homepage',compact('user','menu'));
    }

    public function viewRegister(){
      return view('pages.auth.register-fullscreen');
    }

    public function viewRegisterAlt(){
      return view('pages.auth.register-aside');
    }

    public function viewLogin(){
      return view('pages.auth.login');
    }

    public function viewLostPassword(){
      return view('pages.auth.lost-password');
    }

    public function viewNewPassword(){
      return view('pages.auth.new-password');
    }

}
