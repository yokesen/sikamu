<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LabController extends Controller
{
    public function imageUploader(){
      $menu = 'getting-started';
      $user = profile();
      return view('lab.image-upload',compact('menu','user'));
    }
}
