<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class DashboardController extends Controller
{
  public function viewDashboard(){
    $menu = 'Dashboard';
    $user = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->first();
    /*-----------------*/

    if($user->id_cms_privileges == 7){
      return redirect()->route('viewGettingStarted');
    }else{
      return view('pages.onboard.dashboard',compact('menu','user'));
    }
  }
}
