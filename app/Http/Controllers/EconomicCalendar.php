<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;

class EconomicCalendar extends Controller
{
    public function calendar(){
      $user = DB::table('users_cabinet')->where('id','4517')->first();
      $menu = 'Economic Calendar';
      /*-----------------*/
      $response = Http::get('https://nfs.faireconomy.media/ff_calendar_thisweek.json');
      $results = $response->json();
      return view('pages.apps.economic-calendar',compact('user','menu','results'));
    }
}
