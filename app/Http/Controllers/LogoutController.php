<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Alert;
class LogoutController extends Controller
{
    public function destroy(){
      Session()->flush();
      Alert::success( 'Kamu Udah logout!!','Cepat balik ke sini lagi ya!!')->showConfirmButton('Okei dokei!', '#DB1430');
      return redirect()->route('viewLogin');
    }
}
