<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Storage;

class ImageUploaderController extends Controller
{
    public function uploadImage(Request $request){
      $uuid = uuid();
      if($request->file('gambar'))
      {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }else{
          $image_normal = Image::make($image)->widen(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$image_name, $image_normal->__toString());
      }
      return $image_name;
    }
}
