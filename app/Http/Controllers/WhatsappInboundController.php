<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WhatsappInboundController extends Controller
{
    public function inbound(Request $request){
      $data = json_decode($request->getContent());
      $appPackageName = $data->appPackageName;
      // package name of messenger to detect which messenger the message comes from
    	$messengerPackageName = $data->messengerPackageName;
    	// name/number of the message sender (like shown in the Android notification)
    	$sender = $data->query->sender;
    	// text of the incoming message
    	$message = $data->query->message;
    	// is the sender a group? true or false
    	$isGroup = $data->query->isGroup;
    	// id of the AutoResponder rule which has sent the web server request
    	$ruleId = $data->query->ruleId;

      $verifyID = explode('verify ',$message);
      $verify = $verifyID[1];

      $insert = DB::table('whatsapp')->insert([
        'content' => $appPackageName.'|'.$messengerPackageName.'|'.$sender.'|'.$message.'|'.$isGroup.'|'.$ruleId,
        'phone' => $sender,
        'message' => $message
      ]);

      $user = DB::table('users_cabinet')->where('uuid',$verify)->first();

      if(!empty($user)){
        $update = DB::table('users_cabinet')->where('uuid',$verify)->update([
          'whatsapp' => $sender
        ]);
        return json_encode(array("replies" => array(
          array("message" => "Halo $user->name, terima kasih atas verifikasi whatsapp di ".env('APP_NAME')),
          array("message" => "Success ✅")
        )));
      }else{
        return json_encode(array("replies" => array(
          array("message" => "Sepertinya kode verifikasi ini ga valid, coba klik lagi dari website, jangan ada yang diubah ya")
        )));
      }

    }
}
