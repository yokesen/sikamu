<?php

function profileCompletion(){
      $user = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->first();
      $profileMeter = 0;
      $profileScore = 0;

      if(!empty($user->username)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if($user->photo != '/images/pp-default.jpg'){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if(!empty($user->name)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if($user->email_verification == 'verified'){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if(!empty($user->whatsapp)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if(!empty($user->dob)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if($user->photoKTP != '/images/upload-default.jpg'){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if($user->photoTabungan != '/images/upload-default.jpg'){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      $profileCompletion = number_format($profileScore/$profileMeter*100,0,'.','.');

      return $profileCompletion;
}

function profile(){
  $user = DB::table('users_cabinet')->where('uuid',Session::get('user')->uuid)->first();

  return $user;
}

function uuid(){
  $uuid = Session::get('user')->uuid;
  return $uuid;
}
