<!--begin::Input group-->
<div class="row mb-6">
  <!--begin::Label-->
  <label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
  <!--end::Label-->
  <!--begin::Col-->
  <div class="col-lg-8 fv-row">
    <!--begin::Row-->
    <div class="row">
      <!--begin::Col-->
      <div class="col-lg-9 fv-row mb-6">
        <input type="text" name="alamat_email" class="form-control form-control-lg form-control-solid" placeholder="Alamat Email" value="{{$user->email}}" required />
      </div>
      <!--end::Col-->
      <!--begin::Col-->
      <div class="col-lg-3 fv-row">
        <button type="button" class="btn btn-danger" name="button" data-bs-toggle="modal" data-bs-target="#kt_modal_verify_email">Verify</button>
      </div>
      <!--end::Col-->
    </div>
    <!--end::Row-->
  </div>
  <!--end::Col-->
</div>
<!--end::Input group-->
