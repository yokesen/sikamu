<!--begin::Step 1-->
<div class="current" data-kt-stepper-element="content">
  <!--begin::Wrapper-->
  <div class="w-100">
    <!--begin::Heading-->
    <div class="pb-10 pb-lg-15">
      <!--begin::Title-->
      <h2 class="fw-bolder d-flex align-items-center text-dark">Verify Your Email and Whatsapp
        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Penting untuk Anda isi sebagai sarana komunikasi."></i>
      </h2>
      <!--end::Title-->
      <!--begin::Notice-->
      <div class="text-gray-400 fw-bold fs-6">If you need more info, please check out
        <a href="#" class="link-danger fw-bolder">Help Page</a>.
      </div>
      <!--end::Notice-->
    </div>
    <!--end::Heading-->
    <!--begin::Input group-->
    <div class="fv-row">
        @if ($user->email_verification == "new")
          @include('pages.onboard.partial._email')
        @endif

    </div>
    <!--end::Input group-->
  </div>
  <!--end::Wrapper-->
</div>
<!--end::Step 1-->

@section('modals')
<!--begin::Modal - Verify email-->
<div class="modal fade" id="kt_modal_verify_email" tabindex="-1" aria-hidden="true">
  <!--begin::Modal dialog-->
  <div class="modal-dialog modal-dialog-centered mw-800px">
    <!--begin::Modal content-->
    <div class="modal-content">
      <!--begin::Modal header-->
      <div class="modal-header pb-0 border-0 justify-content-end">
        <!--begin::Close-->
        <div class="btn btn-sm btn-icon btn-active-color-danger" data-bs-dismiss="modal">
          <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
          <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
              </g>
            </svg>
          </span>
          <!--end::Svg Icon-->
        </div>
        <!--end::Close-->
      </div>
      <!--begin::Modal header-->
      <!--begin::Modal body-->
      <div class="modal-body scroll-y pt-0 pb-15">
        <!--begin::Wrapper-->
        <div class="pt-lg-10">
          <!--begin::Logo-->
          <h1 class="fw-bolder fs-2qx text-dark mb-7">Verify Your Email</h1>
          <!--end::Logo-->
          <!--begin::Message-->
          <div class="fs-3 fw-bold text-gray-400 mb-10">We have sent an email to
            <a href="#" class="link-danger fw-bolder">{{$user->email}}</a>
            <br />please follow a link to verify your email.
          </div>
          <!--end::Message-->
          <!--begin::Action-->
          <div class="text-center mb-10">
            <a href="#" class="btn btn-lg btn-danger fw-bolder" data-bs-dismiss="modal">Skip for now</a>
          </div>
          <!--end::Action-->
          <!--begin::Action-->
          <div class="fs-5">
            <span class="fw-bold text-gray-700">Did’t receive an email?</span>
            <a href="#" class="link-danger fw-bolder">Resend</a>
          </div>
          <!--end::Action-->
        </div>
        <!--end::Wrapper-->
      </div>
      <!--end::Modal body-->
    </div>
    <!--end::Modal content-->
  </div>
  <!--end::Modal dialog-->
</div>
<!--end::Modal - verify email-->

<!--begin::Modal - Verify whatsapp-->
<div class="modal fade" id="kt_modal_verify_whatsapp" tabindex="-1" aria-hidden="true">
  <!--begin::Modal dialog-->
  <div class="modal-dialog modal-dialog-centered mw-800px">
    <!--begin::Modal content-->
    <div class="modal-content">
      <!--begin::Modal header-->
      <div class="modal-header pb-0 border-0 justify-content-end">
        <!--begin::Close-->
        <div class="btn btn-sm btn-icon btn-active-color-danger" data-bs-dismiss="modal">
          <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
          <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
              </g>
            </svg>
          </span>
          <!--end::Svg Icon-->
        </div>
        <!--end::Close-->
      </div>
      <!--begin::Modal header-->
      <!--begin::Modal body-->
      <div class="modal-body scroll-y pt-0 pb-15">
        <!--begin::Wrapper-->
        <div class="pt-lg-10">
          <!--begin::Logo-->
          <h1 class="fw-bolder fs-2qx text-dark mb-7">Verify Your Whatsapp</h1>
          <!--end::Logo-->
          <!--begin::Message-->
          <div class="fs-3 fw-bold text-gray-400 mb-10">We have sent a code to
            <a href="#" class="link-danger fw-bolder">+62-[no-whatsapp]</a>
            <br />please follow a link to verify your whatsapp.
          </div>
          <!--end::Message-->
          <!--begin::Action-->
          <div class="text-center mb-10">
            <a href="#" class="btn btn-lg btn-danger fw-bolder" data-bs-dismiss="modal">Skip for now</a>
          </div>
          <!--end::Action-->
          <!--begin::Action-->
          <div class="fs-5">
            <span class="fw-bold text-gray-700">Did’t receive the code?</span>
            <a href="#" class="link-danger fw-bolder">Resend</a>
          </div>
          <!--end::Action-->
        </div>
        <!--end::Wrapper-->
      </div>
      <!--end::Modal body-->
    </div>
    <!--end::Modal content-->
  </div>
  <!--end::Modal dialog-->
</div>
<!--end::Modal - verify whatsapp-->
@endsection
