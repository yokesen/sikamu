<!--begin::Step 1-->
<div class="stepper-item {{$user->onboardingStep == 1 ? 'current' : '' }}" data-kt-stepper-element="nav">
  <!--begin::Line-->
  <div class="stepper-line w-40px"></div>
  <!--end::Line-->
  <!--begin::Icon-->
  <div class="stepper-icon w-40px h-40px">
    @if ($user->onboardingStep < 2 )
      <span class="stepper-number">1</span>
    @else
      <i class="fas fa-check text-danger"></i>
    @endif
  </div>
  <!--end::Icon-->
  <!--begin::Label-->
  <div class="stepper-label">
    <h3 class="stepper-title">Credential Verification</h3>
    <div class="stepper-desc fw-bold">Setup Your Account Details</div>
  </div>
  <!--end::Label-->
</div>
<!--end::Step 1-->
<!--begin::Step 2-->
<div class="stepper-item {{$user->onboardingStep == 2 ? 'current' : ''  }}" data-kt-stepper-element="nav">
  <!--begin::Line-->
  <div class="stepper-line w-40px"></div>
  <!--end::Line-->
  <!--begin::Icon-->
  <div class="stepper-icon w-40px h-40px">
    @if ($user->onboardingStep < 3)
      <span class="stepper-number">2</span>
    @else
      <i class="fas fa-check text-danger"></i>
    @endif
  </div>
  <!--end::Icon-->
  <!--begin::Label-->
  <div class="stepper-label">
    <h3 class="stepper-title">Trading Academic</h3>
    <div class="stepper-desc fw-bold">Prepare Your Trading Journey</div>
  </div>
  <!--end::Label-->
</div>
<!--end::Step 2-->
<!--begin::Step 3-->
<div class="stepper-item {{$user->onboardingStep == 3 ? 'current' : ''  }}" data-kt-stepper-element="nav">
  <!--begin::Line-->
  <div class="stepper-line w-40px"></div>
  <!--end::Line-->
  <!--begin::Icon-->
  <div class="stepper-icon w-40px h-40px">
    @if ($user->onboardingStep < 4)
      <span class="stepper-number">3</span>
    @else
      <i class="fas fa-check text-danger"></i>
    @endif
  </div>
  <!--end::Icon-->
  <!--begin::Label-->
  <div class="stepper-label">
    <h3 class="stepper-title">Open Real Account</h3>
    <div class="stepper-desc fw-bold">Setup Your Real Account</div>
  </div>
  <!--end::Label-->
</div>
<!--end::Step 3-->
<!--begin::Step 4-->
<div class="stepper-item {{$user->onboardingStep == 4 ? 'current' : ''  }}" data-kt-stepper-element="nav">
  <!--begin::Line-->
  <div class="stepper-line w-40px"></div>
  <!--end::Line-->
  <!--begin::Icon-->
  <div class="stepper-icon w-40px h-40px">
    @if ($user->onboardingStep < 5)
      <span class="stepper-number">4</span>
    @else
      <i class="fas fa-check text-danger"></i>
    @endif
  </div>
  <!--end::Icon-->
  <!--begin::Label-->
  <div class="stepper-label">
    <h3 class="stepper-title">Deposit</h3>
    <div class="stepper-desc fw-bold">Funding Your Trading Account</div>
  </div>
  <!--end::Label-->
</div>
<!--end::Step 4-->
<!--begin::Step 5-->
<div class="stepper-item {{$user->onboardingStep == 5 ? 'current' : ''  }}" data-kt-stepper-element="nav">
  <!--begin::Line-->
  <div class="stepper-line w-40px"></div>
  <!--end::Line-->
  <!--begin::Icon-->
  <div class="stepper-icon w-40px h-40px">
    @if ($user->onboardingStep != 5)
      <span class="stepper-number">5</span>
    @else
      <i class="fas fa-check text-danger"></i>
    @endif
  </div>
  <!--end::Icon-->
  <!--begin::Label-->
  <div class="stepper-label">
    <h3 class="stepper-title">Completed</h3>
    <div class="stepper-desc fw-bold">Ready to Trade</div>
  </div>
  <!--end::Label-->
</div>
<!--end::Step 5-->
