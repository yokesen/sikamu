@extends('template.master')

@section('title','Economic Calendar')
@section('metadescription','Economic Calendar')
@section('metakeyword','Economic Calendar')
@section('bc-1','Apps')
@section('bc-2','Economic Calendar')

@section('container')

<!--begin::Container-->
<div id="kt_content_container" class="container">
  <div class="row g-5 g-xxl-8">
    <!--begin::Col-->
    <div class="col-xl-12">
      <!--begin::Tables Widget 5-->
      <div class="card card-xxl-stretch mb-5 mb-xxl-8">
        <!--begin::Header-->
        <div class="card-header border-0 pt-5">
          <h3 class="card-title align-items-start flex-column">
            <span class="card-label fw-bolder fs-3 mb-1">Economic Calendar</span>
          </h3>

        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body py-3">
          <div class="tab-content">
            <!--begin::Tap pane-->
            <div class="tab-pane fade show active">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-200 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="border-0">
                      <th class="p-0 min-w-10px text-center">Time</th>
                      <th class="p-0 min-w-20px text-center">Impact</th>
                      <th class="p-0 min-w-140px text-center">Detail</th>
                      <th class="p-0 min-w-110px text-center">Currency</th>
                      <th class="p-0 min-w-50px text-center">Actual</th>
                      <th class="p-0 min-w-50px text-center">Forecast</th>
                      <th class="p-0 min-w-50px text-center">Previous</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody>
                    @foreach ($results as $value)
                    <tr>
                      <td>
                        <span class="text-muted fw-bold d-block">{{date('d-m-Y',strtotime($value['date']))}}</span>
                        <span class="text-muted fw-bold d-block">{{date('H:i',strtotime($value['date']))}} WIB</span>
                      </td>
                      <td>
                        @if($value['impact']=="Low")
                          <span class="badge badge-light-info">{{$value['impact']}}</span>
                        @elseif($value['impact']=="Medium")
                          <span class="badge badge-light-warning">{{$value['impact']}}</span>
                        @elseif($value['impact']=="High")
                          <span class="badge badge-light-danger">{{$value['impact']}}</span>
                        @endif
                      </td>
                      <td>
                        <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">{{$value['title']}}</a>
                      </td>
                      <td class="text-center text-muted fw-bold">{{$value['country']}}</td>
                      <td class="text-center text-muted fw-bold"></td>
                      <td class="text-center text-muted fw-bold">{{$value['forecast']}}</td>
                      <td class="text-center text-muted fw-bold">{{$value['previous']}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
              </div>
              <!--end::Table-->
            </div>
            <!--end::Tap pane-->
          </div>
        </div>
        <!--end::Body-->
      </div>
      <!--end::Tables Widget 5-->
    </div>
  </div>
</div>

@endsection
