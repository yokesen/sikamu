@extends('template.layout.auth.master')

@section('container')
  <!--begin::Body-->
	<body id="kt_body" class="bg-gold header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Authentication - Sign-in -->
			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
				<!--begin::Content-->
				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
          @include('template.layout.auth._logo')
					<!--begin::Wrapper-->
					<div class="w-lg-500px bg-white rounded shadow-sm p-10 p-lg-15 mx-auto">

							<!--begin::Heading-->
							<div class="text-center mb-10">
								<!--begin::Title-->
								<h1 class="text-dark mb-3">Sign In to Orbitrade</h1>
								<!--end::Title-->
								<!--begin::Link-->
								<div class="text-gray-400 fw-bold fs-4">New Here?
								<a href="{{route('viewRegister')}}" class="link-danger fw-bolder">Create an Account</a></div>
								<!--end::Link-->
							</div>
							<!--begin::Heading-->
							<form class="form w-100" method="post" action="{{route('process-Login')}}" id="registerForm">

								<!--begin::Input group-->
								<div class="fv-row mb-7">
									<label class="form-label fw-bolder text-dark fs-6">Email</label>
									<input class="form-control form-control-lg form-control-solid {{ old('email') && !$errors->has('email') ? 'input-valid' : '' }} {{$errors->has('email') ? 'input-error' : ''}}" type="email" placeholder="" name="email" autocomplete="off" value="{{ old('email') }}" required {{$errors->has('email') ? 'autofocus' : ''}}/>
									@if ($errors->has('email'))
											<h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('email') }}</h4>
									@endif
								</div>
								<!--end::Input group-->
									<!--begin::Input group-->
									<div class="fv-row mb-10">
										<!--begin::Wrapper-->
										<div class="d-flex flex-stack mb-2">
											<!--begin::Label-->
											<label class="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
											<!--end::Label-->
											<!--begin::Link-->
											<a href="{{route('viewLostPassword')}}" class="link-danger fs-6 fw-bolder">Forgot Password ?</a>
											<!--end::Link-->
										</div>
										<!--end::Wrapper-->
										<!--begin::Input-->
										<input class="form-control form-control-lg form-control-solid {{old('password') ? 'input-error' : ''}}" type="password" placeholder="" minlength="8"  name="password" autocomplete="off" required/>
										<!--end::Input-->
									</div>
									<!--end::Input group-->

			            @csrf
			            <div class="g-recaptcha" data-sitekey="{{ENV('NOCAPTCHA_SITEKEY')}}"></div>

			            <h4 class="text-danger mt-6" hidden id="recaptcha-error"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> Wajib buktikan kamu bukan robot!</h4>

			            <!--begin::Actions-->
			            <div class="text-center mt-6">
			              <div class="d-grid gap-2">
			                <button type="submit" class="btn btn-lg btn-danger" id="ot_button">
			                    <span class="indicator-label">
			                        Submit
			                    </span>
			                    <span class="indicator-progress">
			                        Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
			                    </span>
			                </button>

			              </div>
			            </div>
			            <!--end::Actions-->
			            <script src="https://www.google.com/recaptcha/api.js?hl=id"></script>
							</form>

								<!--begin::Separator-->
								<div class="text-center text-muted text-uppercase fw-bolder mb-5">or</div>
								<!--end::Separator-->
								<!--begin::Google link-->
								<a href="{{ route('process-login-oAuth', 'google') }}" class="btn btn-flex flex-center btn-light-danger btn-lg w-100 mb-5">
								<img alt="Logo" src="{{url('/')}}/assets/media/svg/brand-logos/google-icon.svg" class="h-20px me-3" />Continue with Google</a>
								<!--end::Google link-->
								<!--begin::Google link-->
								<a href="#" class="btn btn-flex flex-center btn-light-danger btn-lg w-100 mb-5">
								<img alt="Logo" src="{{url('/')}}/assets/media/svg/brand-logos/facebook-4.svg" class="h-20px me-3" />Continue with Facebook</a>
								<!--end::Google link-->

							</div>
							<!--end::Actions-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Content-->
@endsection

@section('jsinline')
	<script src="{{url('/')}}/assets/js/orbitrade/auth/recaptcha.js"></script>
@endsection
