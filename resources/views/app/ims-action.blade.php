@extends('template.master')

@section('title','Getting Started')
@section('metadescription','Onboarding Client')
@section('metakeyword','Onboarding')
@section('bc-1','Dashboard')
@section('bc-2','Onboarding')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._user-card')
    @include('app.partials._profil-view')
  </div>
@endsection
