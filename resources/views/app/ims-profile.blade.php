@extends('template.master')

@section('title','Getting Started')
@section('metadescription','Onboarding Client')
@section('metakeyword','Onboarding')
@section('bc-1','Dashboard')
@section('bc-2','Onboarding')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._user-card')


    @include('app.partials._profil-edit')
  </div>
@endsection

@section('jsonpage')
  <script type="text/javascript">

  var myDropzone = new Dropzone("#ot_ktp", {
      url: "{{route('process-Upload-Ktp')}}", // Set the url for your upload script location
      paramName: "gambar", // The name that will be used to transfer the file
      method: "post",
      headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
      maxFiles: 1,
      maxFilesize: 10, // MB
      addRemoveLinks: true,
      accept: function(file, done) {
        if (file.name == "wow.jpg") {
            done("Naha, you don't.");
        } else {
            done();
        }
      }
    });
  </script>
@endsection
