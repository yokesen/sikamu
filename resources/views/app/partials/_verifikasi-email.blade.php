<div class="row">
  <div class="col-md-6 offset-md-3">
    <!--begin::Engage Widget 1-->
    <div class="card mb-5 mb-xl-10">
      <!--begin::Body-->
      <div class="card-body pb-0">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column justify-content-between h-100">
          <!--begin::Section-->
          <div class="pt-15 mb-10">
            <!--begin::Title-->
            <h3 class="text-dark text-center fs-1 fw-bolder lh-lg">Verifikasi Email,
              <br />untuk mendapatkan akun Trader.</h3>
              <!--end::Title-->
              <!--begin::Text-->
              <div class="text-center text-gray-600 fs-6 fw-bold pt-4 pb-1">Kami telah mengirimkan Email link verifikasi.
                <br />Jika belum menerima, silahkan klik tombol di bawah ini.</div>
                <!--end::Text-->
                <!--begin::Action-->
                <div class="text-center py-7">
                  <a href="{{route('askToVerifyMail')}}" class="btn btn-primary fs-6 px-6">Kirim Ulang Link Verifikasi</a>
                </div>
                <!--end::Action-->
              </div>
              <!--end::Section-->
              <!--begin::Image-->
              <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom card-rounded-bottom h-200px" style="background-image:url('assets/media/illustrations/work.png')"></div>
              <!--end::Image-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Body-->
        </div>
        <!--end::Engage Widget 1-->
  </div>
</div>
