<!--begin::Aside Menu-->
<div class="hover-scroll-overlay-y my-2 py-5 py-lg-8" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
	data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
	<!--begin::Menu-->
	<div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'dashboard' ? 'active' : ''}}" href="{{route('viewDashboard')}}">
				<span class="menu-icon">
					<i class="bi bi-house fs-3"></i>
				</span>
				<span class="menu-title">Dashboard</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'getting-started' ? 'active' : ''}}" href="{{route('viewGettingStarted')}}">
				<span class="menu-icon">
					<i class="bi bi-flag fs-3"></i>
				</span>
				<span class="menu-title">Getting Started</span>
			</a>
		</div>

		<div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">Client Area</span>
			</div>
		</div>
		<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
			<span class="menu-link">
				<span class="menu-icon">
					<i class="bi bi-person fs-2"></i>
				</span>
				<span class="menu-title">Account</span>
				<span class="menu-arrow"></span>
			</span>
			<div class="menu-sub menu-sub-accordion menu-active-bg">
				<div class="menu-item">
					<a class="menu-link" href="#">
						<span class="menu-bullet">
							<span class="bullet bullet-dot"></span>
						</span>
						<span class="menu-title">Download MT5</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="#">
						<span class="menu-bullet">
							<span class="bullet bullet-dot"></span>
						</span>
						<span class="menu-title">Open Real Account</span>
					</a>
				</div>
			</div>
		</div>

		<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
			<span class="menu-link">
				<span class="menu-icon">
					<i class="bi bi-cart fs-2"></i>
				</span>
				<span class="menu-title">Cashier</span>
				<span class="menu-arrow"></span>
			</span>
			<div class="menu-sub menu-sub-accordion menu-active-bg">
				<div class="menu-item">
					<a class="menu-link" href="#">
						<span class="menu-bullet">
							<span class="bullet bullet-dot"></span>
						</span>
						<span class="menu-title">Deposit</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="#">
						<span class="menu-bullet">
							<span class="bullet bullet-dot"></span>
						</span>
						<span class="menu-title">Internal Transfer</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="#">
						<span class="menu-bullet">
							<span class="bullet bullet-dot"></span>
						</span>
						<span class="menu-title">Withdrawal</span>
					</a>
				</div>
			</div>
		</div>

		<div class="menu-item">
			<a class="menu-link" href="#">
				<span class="menu-icon">
					<i class="bi bi-award fs-3"></i>
				</span>
				<span class="menu-title">Promotion</span>
			</a>
		</div>


		<div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">Apps</span>
			</div>
		</div>

		<div class="menu-item">
			<a class="menu-link {{ $menu == 'economic-calendar' ? 'active' : ''}}" href="{{route('viewEconomicCalendar')}}">
				<span class="menu-icon">
					<i class="bi bi-calendar fs-3"></i>
				</span>
				<span class="menu-title">Economic Calendar</span>
			</a>
		</div>

		<div class="menu-item">
			<a class="menu-link" href="#">
				<span class="menu-icon">
					<i class="bi bi-gear fs-3"></i>
				</span>
				<span class="menu-title">Trading Tools</span>
			</a>
		</div>

		<div class="menu-item">
			<a class="menu-link" href="#">
				<span class="menu-icon">
					<i class="bi bi-book fs-3"></i>
				</span>
				<span class="menu-title">Trading Academy</span>
			</a>
		</div>

		<div class="menu-item">
			<div class="menu-content">
				<div class="separator mx-1 my-4"></div>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link" href="#">
				<span class="menu-icon">
					<i class="bi bi-grid fs-3"></i>
				</span>
				<span class="menu-title">About</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link" href="#">
				<span class="menu-icon">
					<i class="bi bi-box fs-3"></i>
				</span>
				<span class="menu-title">Getting Started</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link" href="#">
				<span class="menu-icon">
					<i class="bi bi-card-text fs-3"></i>
				</span>
				<span class="menu-title">Privacy Policy</span>
			</a>
		</div>
	</div>
	<!--end::Menu-->
</div>