<div class="hover-scroll-overlay-y my-2 py-5 py-lg-8" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
<!--begin::Menu-->
	<div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
		@foreach (Session::get('menu') as $m)

			@if ($m->color == "red")
				<div class="menu-item">
					<div class="menu-content pt-8 pb-2">
						<span class="menu-section text-muted text-uppercase fs-8 ls-1">{{$m->name}}</span>
					</div>
				</div>
			@elseif($m->color == "normal")
				@if($m->path == "#")
					<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
						<span class="menu-link">
							<span class="menu-icon">
								<i class="{{$m->icon}} fs-2"></i>
							</span>
							<span class="menu-title">{{$m->name}}</span>
							<span class="menu-arrow"></span>
						</span>
						@php
						$child = DB::table('cms_menus')->where('parent_id',$m->id)->where('is_active', 1)->orderby('sorting', 'asc')->get();
						@endphp
						<div class="menu-sub menu-sub-accordion menu-active-bg">
							@foreach ($child as $k)
								<div class="menu-item">
									<a class="menu-link" href="{{route($k->path)}}">
										<span class="menu-bullet">
											<span class="bullet bullet-dot"></span>
										</span>
										<span class="menu-title">{{$k->name}}</span>
									</a>
								</div>
							@endforeach
						</div>
					</div>
				@else
					<div class="menu-item">
						<a class="menu-link {{ $menu == $m->name ? 'active' : ''}}" href="{{route($m->path)}}">
							<span class="menu-icon">
								<i class="{{$m->icon}} fs-3"></i>
							</span>
							<span class="menu-title">{{$m->name}}</span>
						</a>
					</div>
				@endif
			@endif

		@endforeach
	</div>
<!--end::Menu-->
</div>
