
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root" id="app">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">

				@include('template.layout.aside._base')

				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">

					@include('template.layout.header._base')

					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

						@include('template.layout.toolbars._toolbar-1')

						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">

							@yield('container')

						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->

					@include('template.layout._footer')

				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		<!--begin::Drawers-->

		{{--@include('template.layout.topbar.partials._activity-drawer')--}}

		<!--begin::Modals-->

		@yield('modals')

		<!--end::Modals-->
		@include('template.layout._scrolltop')
		<!--end::Main-->
