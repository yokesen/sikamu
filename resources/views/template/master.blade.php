<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="">
		<meta charset="utf-8" />
		<title>@yield('title') | {{ENV('APP_NAME')}}</title>
		<meta name="description" content="@yield('metadescription')" />
		<meta name="keywords" content="@yield('metakeyword')" />
		<link rel="canonical" href="{{url()->full()}}" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="{{url('/')}}/images/favicon.png" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="{{url('/')}}/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{url('/')}}/assets-1/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{url('/')}}/assets/css/custom.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
		<!--begin::Page Custom CSS(used by this page)-->
		@yield('cssinline')
		@yield('cssonpage')
		<!--end::Page Custom CSS-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">

			@include('template.layout.index')


		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="{{url('/')}}/assets/plugins/global/plugins.bundle.js"></script>
		<script src="{{url('/')}}/assets/js/scripts.bundle.js"></script>
		@include('sweetalert::alert')

		<!--end::Global Javascript Bundle-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="{{url('/')}}/assets/js/custom/widgets.js"></script>
		<script src="{{url('/')}}/assets/js/custom/apps/chat/chat.js"></script>
		<script src="{{url('/')}}/assets/js/custom/modals/create-app.js"></script>
		<script src="{{url('/')}}/assets/js/custom/modals/upgrade-plan.js"></script>
		<script src="{{url('/')}}/assets/js/custom/intro.js"></script>
		@yield('jsinline')
		@yield('jsonpage')
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->

</html>
