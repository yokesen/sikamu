<!--begin::Step 4-->
<div data-kt-stepper-element="content">
  <!--begin::Wrapper-->
  <div class="w-100">
    <!--begin::Heading-->
    <div class="pb-10 pb-lg-15">
      <!--begin::Title-->
      <h2 class="fw-bolder text-dark">Deposit Request</h2>
      <!--end::Title-->
      <!--begin::Notice-->
      <div class="text-gray-400 fw-bold fs-6">If you need more info, please check out
        <a href="#" class="text-danger fw-bolder">Help Page</a>.
      </div>
      <!--end::Notice-->
    </div>
    <!--end::Heading-->
    <!--begin::Input group-->
    <div class="row mb-6">
      <!--begin::Label-->
      <label class="col-lg-4 col-form-label required fw-bold fs-6">From Bank</label>
      <!--end::Label-->
      <!--begin::Col-->
      <div class="col-lg-8">

        <input type="text" name="fname" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="" value="" />

      </div>
      <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-6">
      <!--begin::Label-->
      <label class="col-lg-4 col-form-label required fw-bold fs-6">From Acc. Number</label>
      <!--end::Label-->
      <!--begin::Col-->
      <div class="col-lg-8">

        <input type="text" name="fname" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="" value="" />

      </div>
      <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-6">
      <!--begin::Label-->
      <label class="col-lg-4 col-form-label required fw-bold fs-6">From Acc. Name</label>
      <!--end::Label-->
      <!--begin::Col-->
      <div class="col-lg-8">

        <input type="text" name="fname" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="{{$user->name}}" value="" />

      </div>
      <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-6">
      <!--begin::Label-->
      <label class="col-lg-4 col-form-label required fw-bold fs-6">Amount</label>
      <!--end::Label-->
      <!--begin::Col-->
      <div class="col-lg-8">

        <!--begin::Row-->
        <div class="row">
          <!--begin::Col-->
          <div class="col-xs-3 fv-row">
            IDR
          </div>
          <!--end::Col-->
          <!--begin::Col-->
          <div class="col-xs-9 fv-row">
            <input type="text" name="company" class="form-control form-control-lg form-control-solid" placeholder="" value="" />
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->

      </div>
      <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-6">
      <!--begin::Label-->
      <label class="col-lg-4 col-form-label required fw-bold fs-6">To Trading Account</label>
      <!--end::Label-->
      <!--begin::Col-->
      <div class="col-lg-8">

        <select class="form-control form-control-lg form-control-solid" name="nama-bank">
          <option value="">Klik di sini</option>
          <option value="">-----------------------------</option>
          <option value="1">777290001 | Startup</option>
          <option value="1">999120071 | Advanced</option>
        </select>

      </div>
      <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="fv-row">
      <!--begin::Dropzone-->
      <div class="dropzone" id="kt_dropzonejs_example_1">
        <!--begin::Message-->
        <div class="dz-message needsclick">
          <!--begin::Icon-->
          <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
          <!--end::Icon-->

          <!--begin::Info-->
          <div class="ms-4">
            <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Drop files here or click to upload.</h3>
            <span class="fs-7 fw-bold text-gray-400">Upload Bukti Transfer</span>
          </div>
          <!--end::Info-->
        </div>
      </div>
      <!--end::Dropzone-->
    </div>
    <!--end::Input group-->
  </div>
  <!--end::Wrapper-->
</div>
<!--end::Step 4-->
