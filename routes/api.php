<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiClientController;
use App\Http\Controllers\ApiMySessionController;
use App\Http\Controllers\ApiBankListController;
use App\Http\Controllers\GettingStartedController;
use App\Http\Controllers\WhatsappInboundController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v3')->group(function () {
  Route::get('/client/m/session',[ApiMySessionController::class,'mySession']);
  Route::get('/bank/list',[ApiBankListController::class,'bankList']);

  Route::post('/getting-started',[GettingStartedController::class,'SubmitGettingStarted']);
  Route::post('/lab/wa/inbound',[WhatsappInboundController::class,'inbound'])->name('labWaInbound');
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
