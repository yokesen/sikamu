<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\EconomicCalendar;
use App\Http\Controllers\LoginSocialite;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GettingStartedController;
use App\Http\Controllers\MailVerificationController;
use App\Http\Controllers\ApiMySessionController;
use App\Http\Controllers\AuthRegisterController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\WhatsappInboundController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ImageUploaderController;
use App\Http\Controllers\LabController;


/*
|--------------------------------------------------------------------------
| VIEW ROUTES
|--------------------------------------------------------------------------
|
*/
Route::get('/', [PageController::class,'viewRegister'])->name('viewHomepage');
Route::get('/register', [PageController::class,'viewRegister'])->name('viewRegister');
Route::get('/register-alt', [PageController::class,'viewRegisterAlt'])->name('viewRegisterAlt');
Route::get('/login', [PageController::class,'viewLogin'])->name('viewLogin');
Route::get('/lost-password', [PageController::class,'viewLostPassword'])->name('viewLostPassword');
Route::get('/new-password', [PageController::class,'viewNewPassword'])->name('viewNewPassword');
Route::get('/onboarding/mail/{email}/verify/{code}', [MailVerificationController::class,'verifyActionMail'])->name('verifyActionMail');
Route::get('/privacy-policy',function () {return view('desktop.privacy-policy');})->name('privacy-policy');
/*
|--------------------------------------------------------------------------
| VIEW ROUTES WITH AUTH
|--------------------------------------------------------------------------
|
*/
Route::group(['middleware' => 'auth'], function() {
    Route::get('/dashboard', [DashboardController::class,'viewDashboard'])->name('viewDashboard');
    /*ONBOARDING*/
    Route::get('/getting-started', [GettingStartedController::class,'viewGettingStarted'])->name('viewGettingStarted');
    Route::get('/onboarding/mail/send-code', [MailVerificationController::class,'askToVerifyMail'])->name('askToVerifyMail');
    Route::get('/app/do/onboarding', [GettingStartedController::class,'appDoOnboard'])->name('viewAppDoOnboard');
    Route::get('/app/do/profile/edit', [GettingStartedController::class,'editProfile'])->name('viewAppDoEditProfile');
    Route::get('/app/do/bank/edit', [GettingStartedController::class,'editBank'])->name('viewAppDoEditBank');
    /*ONBOARDING*/
    Route::get('/profile', [PageController::class,'index'])->name('viewProfile');
    Route::get('/referral', [PageController::class,'index'])->name('viewReferral');
    Route::get('/account/download-mt5', [PageController::class,'index'])->name('viewDownloadMt5');
    Route::get('/account/open-real-account', [PageController::class,'index'])->name('viewOpenRealAccount');
    Route::get('/cashier/deposit', [PageController::class,'index'])->name('viewDeposit');
    Route::get('/cashier/withdrawal', [PageController::class,'index'])->name('viewWithdrawal');
    Route::get('/promotion', [PageController::class,'index'])->name('viewPromotion');
    Route::get('/apps/economic-calendar', [EconomicCalendar::class,'calendar'])->name('viewEconomicCalendar');
    Route::get('/apps/trading-tools', [PageController::class,'index'])->name('viewTradingTools');
    Route::get('/apps/trading-academy', [PageController::class,'index'])->name('viewTradingAcademic');
    /*SESSION*/
    Route::get('/client/m/session',[ApiMySessionController::class,'mySession']);
    Route::get('/client/m/logout',[LogoutController::class,'destroy'])->name('logout');
    /*LAB*/
    Route::get('/lab/image/uploader',[LabController::class,'imageUploader']);

});

/*
|--------------------------------------------------------------------------
| PROCESS ROUTES
|--------------------------------------------------------------------------
|
*/

Route::get('login/oAuth/process/{provider}', [LoginSocialite::class,'redirectToSocialite'])->name('process-login-oAuth');
Route::get('login/oAuth/callback/{provider}', [LoginSocialite::class,'handleSocialiteCallback'])->name('process-callback-oAuth');
Route::get('login/oAuth/failed/{provider}', [LoginSocialite::class,'handleSocialiteFailed'])->name('process-failed-oAuth');
Route::get('login/oAuth/unsubscribe/{provider}', [LoginSocialite::class,'handleSocialiteUnsubscribe'])->name('process-unsubscribe-oAuth');

Route::post('/auth/register',[AuthRegisterController::class,'submitRegister'])->name('process-Register');
Route::post('/auth/login',[LoginController::class,'submitLogin'])->name('process-Login');
Route::post('/profile/edit',[ProfileController::class,'editProfilePost'])->name('process-Edit-Profile');
Route::post('/upload/bukuTabungan',[ProfileController::class,'uploadBukuTabungan'])->name('process-Upload-Tabungan');
Route::post('/upload/Ktp',[ProfileController::class,'uploadKtp'])->name('process-Upload-Ktp');
Route::post('/upload/image',[ImageUploaderController::class,'uploadImage'])->name('process-Upload-Image');

Route::group(['middleware' => 'auth'], function() {
  Route::post('/p/v3/getting-started',[GettingStartedController::class,'SubmitGettingStarted']);
  Route::post('/p/v3/verify-mail',[GettingStartedController::class,'sendVerifyEmail']);
  Route::post('/p/v3/verifying-mail',[GettingStartedController::class,'verifiyingEmail']);
});
/*
|--------------------------------------------------------------------------
| LAB ROUTES
|--------------------------------------------------------------------------
|
*/
Route::get('/lab/vue',function () {return view('lv.index');})->name('labVue');
