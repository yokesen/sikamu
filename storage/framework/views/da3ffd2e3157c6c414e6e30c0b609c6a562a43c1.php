

<?php $__env->startSection('title','Economic Calendar'); ?>
<?php $__env->startSection('metadescription','Economic Calendar'); ?>
<?php $__env->startSection('metakeyword','Economic Calendar'); ?>
<?php $__env->startSection('bc-1','Apps'); ?>
<?php $__env->startSection('bc-2','Economic Calendar'); ?>

<?php $__env->startSection('container'); ?>

<!--begin::Container-->
<div id="kt_content_container" class="container">
  <div class="row g-5 g-xxl-8">
    <!--begin::Col-->
    <div class="col-xl-12">
      <!--begin::Tables Widget 5-->
      <div class="card card-xxl-stretch mb-5 mb-xxl-8">
        <!--begin::Header-->
        <div class="card-header border-0 pt-5">
          <h3 class="card-title align-items-start flex-column">
            <span class="card-label fw-bolder fs-3 mb-1">Economic Calendar</span>
          </h3>

        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body py-3">
          <div class="tab-content">
            <!--begin::Tap pane-->
            <div class="tab-pane fade show active">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-200 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="border-0">
                      <th class="p-0 min-w-10px text-center">Time</th>
                      <th class="p-0 min-w-20px text-center">Impact</th>
                      <th class="p-0 min-w-140px text-center">Detail</th>
                      <th class="p-0 min-w-110px text-center">Currency</th>
                      <th class="p-0 min-w-50px text-center">Actual</th>
                      <th class="p-0 min-w-50px text-center">Forecast</th>
                      <th class="p-0 min-w-50px text-center">Previous</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody>
                    <?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td>
                        <span class="text-muted fw-bold d-block"><?php echo e(date('d-m-Y',strtotime($value['date']))); ?></span>
                        <span class="text-muted fw-bold d-block"><?php echo e(date('H:i',strtotime($value['date']))); ?> WIB</span>
                      </td>
                      <td>
                        <?php if($value['impact']=="Low"): ?>
                          <span class="badge badge-light-info"><?php echo e($value['impact']); ?></span>
                        <?php elseif($value['impact']=="Medium"): ?>
                          <span class="badge badge-light-warning"><?php echo e($value['impact']); ?></span>
                        <?php elseif($value['impact']=="High"): ?>
                          <span class="badge badge-light-danger"><?php echo e($value['impact']); ?></span>
                        <?php endif; ?>
                      </td>
                      <td>
                        <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6"><?php echo e($value['title']); ?></a>
                      </td>
                      <td class="text-center text-muted fw-bold"><?php echo e($value['country']); ?></td>
                      <td class="text-center text-muted fw-bold"></td>
                      <td class="text-center text-muted fw-bold"><?php echo e($value['forecast']); ?></td>
                      <td class="text-center text-muted fw-bold"><?php echo e($value['previous']); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <!--end::Table body-->
                </table>
              </div>
              <!--end::Table-->
            </div>
            <!--end::Tap pane-->
          </div>
        </div>
        <!--end::Body-->
      </div>
      <!--end::Tables Widget 5-->
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/devims1129cyou/public_html/resources/views/pages/apps/economic-calendar.blade.php ENDPATH**/ ?>