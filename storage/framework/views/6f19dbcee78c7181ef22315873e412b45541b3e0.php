<div class="row">
  <div class="col-md-6">

    <div class="card mb-5 mb-xl-10">
      <div class="card-body p-9">
        <!--begin::Notice-->
        <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed p-6">
          <!--begin::Icon-->
          <!--begin::Svg Icon | path: icons/duotone/Code/Warning-1-circle.svg-->
          <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
              <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
              <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
            </svg>
          </span>
          <!--end::Svg Icon-->
          <!--end::Icon-->
          <!--begin::Wrapper-->
          <div class="d-flex flex-stack flex-grow-1">
            <!--begin::Content-->
            <div class="fw-bold">
              <h4 class="text-gray-800 fw-bolder">Hi <?php echo e(Session::get('user')->username ? Session::get('user')->username : Session::get('user')->name); ?>!</h4>
              <div class="fs-6 text-gray-600">Tolong bantu lengkapi profil-mu ya, karena dibutuhkan beberapa verifikasi untuk Kamu bisa mulai trading.
                <a class="fw-bolder" href="<?php echo e(route('viewAppDoEditProfile')); ?>">Klik di sini untuk edit profil Kamu</a>.
              </div>
            </div>
            <!--end::Content-->
          </div>
          <!--end::Wrapper-->
          </div>
          <!--end::Notice-->
      </div>
    </div>

    <!--begin::details View-->
    <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
      <!--begin::Card header-->
      <div class="card-header cursor-pointer">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Profile Details</h3>
        </div>
        <!--end::Card title-->
        <!--begin::Action-->
        <a href="<?php echo e(route('viewAppDoEditProfile')); ?>" class="btn btn-primary align-self-center">Edit Profile</a>
        <!--end::Action-->
      </div>
      <!--begin::Card header-->
      <!--begin::Card body-->
      <div class="card-body p-9">
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Username</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            <?php if(Session::get('user')->username): ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> <?php echo e(Session::get('user')->username); ?></span>
            <?php else: ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Pilih username-mu."></i></span>
            <?php endif; ?>
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Full Name</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            <?php if(Session::get('user')->name): ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> <?php echo e(Session::get('user')->name); ?></span>
            <?php else: ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Nama lengkap sesuai KTP."></i></span>
            <?php endif; ?>
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Email</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            <?php if(Session::get('user')->email && Session::get('user')->email_verification == 'verified'): ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> <?php echo e(Session::get('user')->email); ?></span>
            <?php elseif(Session::get('user')->email && Session::get('user')->email_verification != 'verified'): ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Verifikasikan Email-mu, karena semua akun trading dan statementnya akan dikirimkan ke sini."></i> <?php echo e(Session::get('user')->email); ?></span>
            <?php else: ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Email tidak boleh kosong."></i></span>
            <?php endif; ?>
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Whatsapp</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            <?php if(Session::get('user')->phone && Session::get('user')->whatsapp): ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> <?php echo e(Session::get('user')->whatsapp); ?></span>
            <?php elseif(Session::get('user')->phone && empty(Session::get('user')->whatsapp)): ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Verifikasi whatsapp-mu untuk mendapatkan sinyal trading dan berbagai macam manfaat lainnya."></i> <?php echo e(Session::get('user')->phone); ?></span>
            <?php else: ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Verifikasi whatsapp-mu untuk mendapatkan sinyal trading dan berbagai macam manfaat lainnya."></i></span>
            <?php endif; ?>
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Tanggal Lahir</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            <?php if(Session::get('user')->dob): ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> <?php echo e(Session::get('user')->dob); ?></span>
            <?php else: ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Minimal 18 tahun ke atas."></i></span>
            <?php endif; ?>
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->


        </div>
        <!--end::Card body-->
      </div>
      <!--end::details View-->
  </div>
  <div class="col-md-6">
    <!--begin::details View-->
    <div class="card mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header cursor-pointer">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Bank</h3>
        </div>
        <!--end::Card title-->
        <!--begin::Action-->
        <a href="<?php echo e(route('viewAppDoEditBank')); ?>" class="btn btn-primary align-self-center">Edit Bank</a>
        <!--end::Action-->
      </div>
      <!--begin::Card header-->
      <!--begin::Card body-->
      <div class="card-body p-9">
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Nama Bank</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            Bank Anu
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Nomor Rekening</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            098726253637
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Nama di Rekening</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            <?php if(Session::get('user')->name): ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> <?php echo e(Session::get('user')->name); ?></span>
            <?php else: ?>
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Nama lengkap sesuai KTP."></i></span>
            <?php endif; ?>
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
      </div>
      <!--end::Card body-->
    </div>
    <!--end::details View-->

    <!--begin::List Widget 7-->
		<div class="card card-xl-stretch mb-xl-8">
			<!--begin::Header-->
			<div class="card-header align-items-center border-0 mt-4">
				<h3 class="card-title align-items-start flex-column">
					<span class="fw-bolder text-dark">Dokumen Syarat</span>
					<span class="text-muted mt-1 fw-bold fs-7">verifikasi identitas dan bank</span>
				</h3>
			</div>
			<!--end::Header-->
			<!--begin::Body-->
			<div class="card-body pt-3">
				<!--begin::Item-->
				<div class="d-flex align-items-sm-center mb-7">
					<!--begin::Symbol-->
					<div class="symbol symbol-60px symbol-2by3 me-4">
            <?php if($user->photoKTP != '/images/upload-default.jpg'): ?>
              <div class="symbol-label" style="background-image: url('<?php echo e(env('IMG_USER').$user->photoKTP); ?>')"></div>
            <?php else: ?>
              <div class="symbol-label" style="background-image: url('/images/upload-default.jpg')"></div>
            <?php endif; ?>
					</div>
					<!--end::Symbol-->
					<!--begin::Title-->
					<div class="d-flex flex-row-fluid flex-wrap align-items-center">
						<div class="flex-grow-1 me-2">
							<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">KTP/SIM/Paspor</a>
							<span class="text-muted fw-bold d-block pt-1"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> Valid</span>
						</div>
            <span class="badge badge-light-warning fs-8 fw-bolder my-2">In Progress</span>
					</div>
					<!--end::Title-->
				</div>
				<!--end::Item-->
				<!--begin::Item-->
				<div class="d-flex align-items-sm-center mb-7">
					<!--begin::Symbol-->
					<div class="symbol symbol-60px symbol-2by3 me-4">
            <?php if($user->photoTabungan != '/images/upload-default.jpg'): ?>
              <div class="symbol-label" style="background-image: url('<?php echo e(env('IMG_USER').$user->photoTabungan); ?>')"></div>
            <?php else: ?>
              <div class="symbol-label" style="background-image: url('/images/upload-default.jpg')"></div>
            <?php endif; ?>
					</div>
					<!--end::Symbol-->
					<!--begin::Title-->
					<div class="d-flex flex-row-fluid flex-wrap align-items-center">
						<div class="flex-grow-1 me-2">
							<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">Cover Buku Tabungan</a>
							<span class="text-muted fw-bold d-block pt-1"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> Valid</span>
						</div>
						<span class="badge badge-light-warning fs-8 fw-bolder my-2">In Progress</span>
					</div>
					<!--end::Title-->
				</div>
				<!--end::Item-->
			</div>
			<!--end::Body-->
		</div>
		<!--end::List Widget 7-->
  </div>
</div>
<?php /**PATH /home/devims1129cyou/public_html/resources/views/app/partials/_profil-view.blade.php ENDPATH**/ ?>