<div class="hover-scroll-overlay-y my-2 py-5 py-lg-8" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
<!--begin::Menu-->
	<div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
		<?php $__currentLoopData = Session::get('menu'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

			<?php if($m->color == "red"): ?>
				<div class="menu-item">
					<div class="menu-content pt-8 pb-2">
						<span class="menu-section text-muted text-uppercase fs-8 ls-1"><?php echo e($m->name); ?></span>
					</div>
				</div>
			<?php elseif($m->color == "normal"): ?>
				<?php if($m->path == "#"): ?>
					<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
						<span class="menu-link">
							<span class="menu-icon">
								<i class="<?php echo e($m->icon); ?> fs-2"></i>
							</span>
							<span class="menu-title"><?php echo e($m->name); ?></span>
							<span class="menu-arrow"></span>
						</span>
						<?php
						$child = DB::table('cms_menus')->where('parent_id',$m->id)->where('is_active', 1)->orderby('sorting', 'asc')->get();
						?>
						<div class="menu-sub menu-sub-accordion menu-active-bg">
							<?php $__currentLoopData = $child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="menu-item">
									<a class="menu-link" href="<?php echo e(route($k->path)); ?>">
										<span class="menu-bullet">
											<span class="bullet bullet-dot"></span>
										</span>
										<span class="menu-title"><?php echo e($k->name); ?></span>
									</a>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					</div>
				<?php else: ?>
					<div class="menu-item">
						<a class="menu-link <?php echo e($menu == $m->name ? 'active' : ''); ?>" href="<?php echo e(route($m->path)); ?>">
							<span class="menu-icon">
								<i class="<?php echo e($m->icon); ?> fs-3"></i>
							</span>
							<span class="menu-title"><?php echo e($m->name); ?></span>
						</a>
					</div>
				<?php endif; ?>
			<?php endif; ?>

		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</div>
<!--end::Menu-->
</div>
<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/layout/aside/_dynamic_menu.blade.php ENDPATH**/ ?>