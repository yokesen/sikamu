

<?php $__env->startSection('cssinline'); ?>
<style media="screen">

</style>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('container'); ?>
  <!--begin::Body-->
<body id="kt_body" class="bg-gold header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
  <!--begin::Main-->
  <div class="d-flex flex-column flex-root">
    <!--begin::Authentication - Sign-up -->
    <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
      <!--begin::Content-->
      <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
        <?php echo $__env->make('template.layout.auth._logo', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!--begin::Title-->

        <!--end::Title-->
        <!--begin::Wrapper-->
        <div class="w-lg-600px bg-white rounded shadow-sm p-10 p-lg-5" style="opacity:0.95;">

            <!--begin::Heading-->
            <div class="mb-10 text-center">
              <!--begin::Title-->
              <h1 class="text-dark mb-3">Create an Account</h1>
              <!--end::Title-->
              <!--begin::Link-->
              <div class="text-gray-400 fw-bold fs-4">Already have an account?
              <a href="<?php echo e(route('viewLogin')); ?>" class="link-danger fw-bolder">Sign in here</a></div>
              <!--end::Link-->
            </div>
            <!--end::Heading-->
            <!--begin::Action-->
            <a href="<?php echo e(route('process-login-oAuth', 'google')); ?>">
              <button type="button" class="btn btn-light-danger fw-bolder w-100 mb-10">
                <img alt="Logo" src="<?php echo e(url('/')); ?>/assets/media/svg/brand-logos/google-icon.svg" class="h-20px me-3" />
                Sign in with Google
              </button>
            </a>


            <button type="button" class="btn btn-light-danger fw-bolder w-100 mb-10">
            <img alt="Logo" src="<?php echo e(url('/')); ?>/assets/media/svg/brand-logos/facebook-4.svg" class="h-20px me-3" />Sign in with Facebook</button>
            <!--end::Action-->
            <!--begin::Separator-->
            <div class="d-flex align-items-center mb-10">
              <div class="border-bottom border-gray-300 mw-50 w-100"></div>
              <span class="fw-bold text-gray-400 fs-7 mx-2">OR</span>
              <div class="border-bottom border-gray-300 mw-50 w-100"></div>
            </div>
            <!--end::Separator-->

          <!--begin::Form-->
          <form class="form w-100" method="post" action="<?php echo e(route('process-Register')); ?>" id="registerForm">
            <!--begin::Input group-->
            <div class="fv-row mb-7">
              <!--begin::Col-->
                <label class="form-label fw-bolder text-dark fs-6">Nama Lengkap</label>
                <input class="form-control form-control-lg form-control-solid <?php echo e(old('name') && !$errors->has('name') ? 'input-valid' : ''); ?> <?php echo e($errors->has('name') ? 'input-error' : ''); ?>" type="text" placeholder="" name="name" autocomplete="off" value="<?php echo e(old('name')); ?>" required <?php echo e($errors->has('name') ? 'autofocus' : ''); ?>/>
                <?php if($errors->has('name')): ?>
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('name')); ?></h4>
                <?php endif; ?>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="fv-row mb-7">
              <label class="form-label fw-bolder text-dark fs-6">Email</label>
              <input class="form-control form-control-lg form-control-solid <?php echo e(old('email') && !$errors->has('email') ? 'input-valid' : ''); ?> <?php echo e($errors->has('email') ? 'input-error' : ''); ?>" type="email" placeholder="" name="email" autocomplete="off" value="<?php echo e(old('email')); ?>" required <?php echo e($errors->has('email') ? 'autofocus' : ''); ?>/>
              <?php if($errors->has('email')): ?>
                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('email')); ?></h4>
              <?php endif; ?>
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="fv-row mb-7">
              <label class="form-label fw-bolder text-dark fs-6">Whatsapp</label>
              <input class="form-control form-control-lg form-control-solid <?php echo e(old('phone') && !$errors->has('phone') ? 'input-valid' : ''); ?> <?php echo e($errors->has('phone') ? 'input-error' : ''); ?>" type="text" id="ot_phone" placeholder="" name="phone" value="<?php echo e(old('phone')); ?>" autocomplete="off" required/>
              <?php if($errors->has('phone')): ?>
                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('phone')); ?></h4>
              <?php endif; ?>
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="mb-10 fv-row" data-kt-password-meter="true">
              <!--begin::Wrapper-->
              <div class="mb-1">
                <!--begin::Label-->
                <label class="form-label fw-bolder text-dark fs-6">Password</label>
                <!--end::Label-->
                <!--begin::Input wrapper-->
                <div class="position-relative mb-3">
                  <input class="form-control form-control-lg form-control-solid <?php echo e(old('password') ? 'input-error' : ''); ?>" type="password" placeholder="" minlength="8"  name="password" autocomplete="off" required/>
                  <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                    <i class="bi bi-eye-slash fs-2"></i>
                    <i class="bi bi-eye fs-2 d-none"></i>
                  </span>
                </div>
                <!--end::Input wrapper-->
                <!--begin::Meter-->
                <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                </div>
                <!--end::Meter-->
              </div>
              <!--end::Wrapper-->
              <!--begin::Hint-->
              <div class="text-muted">Use 8 or more characters with a mix of letters, numbers &amp; symbols.</div>
              <!--end::Hint-->
              <?php if($errors->has('password')): ?>
                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('password')); ?></h4>
              <?php endif; ?>
            </div>
            <!--end::Input group=-->
            <!--begin::Input group-->
            <div class="fv-row mb-5">
              <label class="form-label fw-bolder text-dark fs-6">Confirm Password</label>
              <input class="form-control form-control-lg form-control-solid <?php echo e(old('password') ? 'input-error' : ''); ?>" minlength="8" type="password" placeholder="" name="password_confirmation" autocomplete="off" required />
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="fv-row mb-10">
              <label class="form-check form-check-custom form-check-solid">
                <input class="form-check-input" type="checkbox" name="toc" value="1" required/>
                <span class="form-check-label fw-bold text-gray-700 fs-6">I Agree &amp;
                <a href="#" class="ms-1 link-danger">Terms and conditions</a>.</span>
              </label>
            </div>
            <!--end::Input group-->

            <?php echo csrf_field(); ?>
            <div class="g-recaptcha" data-sitekey="<?php echo e(ENV('NOCAPTCHA_SITEKEY')); ?>"></div>

            <h4 class="text-danger mt-6" hidden id="recaptcha-error"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> Wajib buktikan kamu bukan robot!</h4>

            <!--begin::Actions-->
            <div class="text-center mt-6">
              <div class="d-grid gap-2">
                <button type="submit" class="btn btn-lg btn-danger" id="ot_button">
                    <span class="indicator-label">
                        Submit
                    </span>
                    <span class="indicator-progress">
                        Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                    </span>
                </button>

              </div>
            </div>
            <!--end::Actions-->
            <script src="https://www.google.com/recaptcha/api.js?hl=id"></script>
          </form>
          <!--end::Form-->
        </div>
        <!--end::Wrapper-->
      </div>
      <!--end::Content-->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jsinline'); ?>
  <script src="<?php echo e(url('/')); ?>/assets/js/orbitrade/auth/recaptcha.js"></script>
  <script src="<?php echo e(url('/')); ?>/assets/js/orbitrade/auth/register.js"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.layout.auth.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/devims1129cyou/public_html/resources/views/pages/auth/register-fullscreen.blade.php ENDPATH**/ ?>