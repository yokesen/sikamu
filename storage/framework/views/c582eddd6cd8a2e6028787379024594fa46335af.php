
<!--begin::Toolbar wrapper-->
<div class="d-flex align-items-stretch flex-shrink-0">
	<!--begin::Search-->
	<div class="d-flex align-items-stretch ms-1 ms-lg-3">

		<!--layout-partial:layout/search/_base.html-->

	</div>
	<!--end::Search-->
	<!--begin::Activities-->
	<div class="d-flex align-items-center ms-1 ms-lg-3">
		<!--begin::drawer toggle-->
		<div class="btn btn-icon btn-active-primary w-30px h-30px w-md-40px h-md-40px" id="kt_activities_toggle">
			<i class="bi bi-box-seam fs-3 text-white"></i>
		</div>
		<!--end::drawer toggle-->
	</div>
	<!--end::Activities-->

	<!--begin::Notifications-->
	<div class="d-flex align-items-center ms-1 ms-lg-3">
		<!--begin::Menu-->
		<div class="btn btn-icon btn-active-primary position-relative w-30px h-30px w-md-40px h-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
			<i class="bi bi-app-indicator fs-3 text-white"></i>
			<span class="bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-50 animation-blink"></span>
		</div>

<!--layout-partial:layout/topbar/partials/_notifications-menu.html-->

		<!--end::Menu-->
	</div>
	<!--end::Notifications-->
	<!--begin::User-->
	<div class="d-flex align-items-center ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
		<!--begin::Menu-->
		<div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
			<img src="<?php echo e(Session::get('user')->photo); ?>" alt="<?php echo e(Session::get('user')->name); ?>" />
		</div>

		<?php echo $__env->make('template.layout.topbar.partials._user-menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		<!--end::Menu-->
	</div>
	<!--end::User -->
	<!--begin::Heaeder menu toggle-->
	<?php if(Session::get('user')->id_cms_privileges == 8 || Session::get('user')->id_cms_privileges == 9 || Session::get('user')->id_cms_privileges == 10): ?>
		<div class="d-flex align-items-center d-lg-none ms-2 me-n3" title="Show header menu">
			<div class="btn btn-icon btn-active-light-primary" id="kt_header_menu_mobile_toggle">
				<i class="bi bi-text-left fs-1 text-white"></i>
			</div>
		</div>
	<?php endif; ?>
	<!--end::Heaeder menu toggle-->
</div>
<!--end::Toolbar wrapper-->
<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/layout/topbar/_base.blade.php ENDPATH**/ ?>