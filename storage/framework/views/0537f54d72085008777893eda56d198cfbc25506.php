<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="">
		<meta charset="utf-8" />
		<title><?php echo $__env->yieldContent('title'); ?> | <?php echo e(ENV('APP_NAME')); ?></title>
		<meta name="description" content="<?php echo $__env->yieldContent('metadescription'); ?>" />
		<meta name="keywords" content="<?php echo $__env->yieldContent('metakeyword'); ?>" />
		<link rel="canonical" href="<?php echo e(url()->full()); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="<?php echo e(url('/')); ?>/images/favicon.png" />
		<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="<?php echo e(url('/')); ?>/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo e(url('/')); ?>/assets-1/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo e(url('/')); ?>/assets/css/custom.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
		<!--begin::Page Custom CSS(used by this page)-->
		<?php echo $__env->yieldContent('cssinline'); ?>
		<?php echo $__env->yieldContent('cssonpage'); ?>
		<!--end::Page Custom CSS-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">

			<?php echo $__env->make('template.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="<?php echo e(url('/')); ?>/assets/plugins/global/plugins.bundle.js"></script>
		<script src="<?php echo e(url('/')); ?>/assets/js/scripts.bundle.js"></script>
		<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		<!--end::Global Javascript Bundle-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="<?php echo e(url('/')); ?>/assets/js/custom/widgets.js"></script>
		<script src="<?php echo e(url('/')); ?>/assets/js/custom/apps/chat/chat.js"></script>
		<script src="<?php echo e(url('/')); ?>/assets/js/custom/modals/create-app.js"></script>
		<script src="<?php echo e(url('/')); ?>/assets/js/custom/modals/upgrade-plan.js"></script>
		<script src="<?php echo e(url('/')); ?>/assets/js/custom/intro.js"></script>
		<?php echo $__env->yieldContent('jsinline'); ?>
		<?php echo $__env->yieldContent('jsonpage'); ?>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->

</html>
<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/master.blade.php ENDPATH**/ ?>