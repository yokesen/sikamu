<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
    <meta charset="utf-8" />
		<title><?php echo e(ENV('APP_NAME')); ?></title>
		<meta name="description" content="<?php echo $__env->yieldContent('metadescription'); ?>" />
		<meta name="keywords" content="<?php echo $__env->yieldContent('metakeyword'); ?>" />
		<link rel="canonical" href="<?php echo e(url()->full()); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
		<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
		<?php $config = (new \LaravelPWA\Services\ManifestService)->generate(); echo $__env->make( 'laravelpwa::meta' , ['config' => $config])->render(); ?>
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="<?php echo e(url('/')); ?>/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(url('/')); ?>/assets-1/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo e(url('/')); ?>/assets/css/custom.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
		<?php echo $__env->yieldContent('cssinline'); ?>
	</head>
	<!--end::Head-->
  <?php echo $__env->yieldContent('container'); ?>
					<!--begin::Footer-->
					<div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
						<!--begin::Links-->
						<div class="d-flex flex-center fw-bold fs-6">
							<a href="#" class="text-muted text-hover-primary px-2" target="_blank">About</a>
							<a href="#" class="text-muted text-hover-primary px-2" target="_blank">Support</a>
						</div>
						<!--end::Links-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Body-->
			</div>
			<!--end::Authentication - Sign-up-->
		</div>
		<!--end::Main-->
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="<?php echo e(url('/')); ?>/assets/plugins/global/plugins.bundle.js"></script>
		<script src="<?php echo e(url('/')); ?>/assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<!--end::Javascript-->
		<?php echo $__env->yieldContent('jsinline'); ?>
		<script type="text/javascript">
			$.ajaxSetup({
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
		</script>
	</body>
	<!--end::Body-->
</html>
<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/layout/auth/master.blade.php ENDPATH**/ ?>