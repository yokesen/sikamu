<div class="row">

  <div class="col-md-6">
    <!--begin::Basic info-->
    <div class="card mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Bank </h3>
        </div>
        <!--end::Card title-->
      </div>
      <!--begin::Card header-->
      <!--begin::Content-->
      <div id="kt_account_profile_details" class="collapse show">
        <!--begin::Form-->
        <form id="kt_account_profile_details_form" method="POST" action="<?php echo e(route('process-Edit-Profile')); ?>" class="form" enctype="multipart/form-data">
          <!--begin::Card body-->
          <div class="card-body border-top p-9">

            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Bank</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid <?php echo e(old('username') && !$errors->has('username') ? 'input-valid' : ''); ?> <?php echo e($errors->has('username') ? 'input-error' : ''); ?>" type="text" placeholder="" name="username" autocomplete="off" value="<?php echo e(old('username') ? old('username') : $user->username); ?>" required <?php echo e($errors->has('username') ? 'autofocus' : ''); ?>/>
                <?php if($errors->has('username')): ?>
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('name')); ?></h4>
                <?php endif; ?>
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nomor Rekening</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid <?php echo e(old('username') && !$errors->has('username') ? 'input-valid' : ''); ?> <?php echo e($errors->has('username') ? 'input-error' : ''); ?>" type="text" placeholder="" name="username" autocomplete="off" value="<?php echo e(old('username') ? old('username') : $user->username); ?>" required <?php echo e($errors->has('username') ? 'autofocus' : ''); ?>/>
                <?php if($errors->has('username')): ?>
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('name')); ?></h4>
                <?php endif; ?>
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama di Rekening</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid <?php echo e(old('name') && !$errors->has('name') ? 'input-valid' : ''); ?> <?php echo e($errors->has('name') ? 'input-error' : ''); ?>" type="text" placeholder="" name="name" autocomplete="off" value="<?php echo e(old('name') ? old('name') : $user->name); ?>" required <?php echo e($errors->has('name') ? 'autofocus' : ''); ?>/>
                <?php if($errors->has('name')): ?>
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('name')); ?></h4>
                <?php endif; ?>
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
          </div>
          <!--end::Card body-->
          <?php echo csrf_field(); ?>
          <!--begin::Actions-->
          <div class="card-footer d-flex justify-content-end py-6 px-9">
            <a href="<?php echo e(route('viewGettingStarted')); ?>" type="reset" class="btn btn-white btn-active-light-primary me-2">Discard</a>
            <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
          </div>
          <!--end::Actions-->
        </form>
        <!--end::Form-->
      </div>
      <!--end::Content-->
    </div>
  </div>
  <div class="col-md-6">

    <!--begin::Basic info-->
    <div class="card mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Dokumen Buku Tabungan </h3>
        </div>
        <!--end::Card title-->
      </div>
      <!--begin::Card header-->
      <!--begin::Content-->
      <div class="collapse show">
        <!--begin::Form-->
        <form class="form" action="#" method="post">
          <!--begin::Card body-->
          <div class="card-body border-top p-9">

            <!--begin::Input group-->
            <div class="fv-row">
              <!--begin::Dropzone-->
              <div class="dropzone" id="ot_buku_rekening">
                <!--begin::Message-->
                <div class="dz-message needsclick">
                  <!--begin::Icon-->
                  <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                  <!--end::Icon-->

                  <!--begin::Info-->
                  <div class="ms-4">
                    <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Klik di sini untuk upload.</h3>
                    <span class="fs-7 fw-bold text-gray-400">Upload foto cover buku tabungan</span>
                  </div>
                  <!--end::Info-->
                </div>
              </div>
              <!--end::Dropzone-->
            </div>
            <!--end::Input group-->
          </div>
          <!--end::Card body-->
        </form>
        <!--end::Form-->
      </div>
    </div>
  </div>
</div>
<?php /**PATH /home/devims1129cyou/public_html/resources/views/app/partials/_bank-edit.blade.php ENDPATH**/ ?>