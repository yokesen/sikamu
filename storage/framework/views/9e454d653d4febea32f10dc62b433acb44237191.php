<!--begin::Step 5-->
<div data-kt-stepper-element="content">
  <!--begin::Wrapper-->
  <div class="w-100">
    <!--begin::Heading-->
    <div class="pb-8 pb-lg-10">
      <!--begin::Title-->
      <h2 class="fw-bolder text-dark">Your Are Done!</h2>
      <!--end::Title-->

    </div>
    <!--end::Heading-->
    <!--begin::Body-->
    <div class="mb-0">
      <!--begin::Text-->
      <div class="fs-6 text-gray-600 mb-5">Amazing, you've successfully graduate from our onboarding process.</div>
      <!--end::Text-->
      <!--begin::Alert-->
      <!--begin::Notice-->
      <div class="notice d-flex bg-light-success rounded border-success border border-dashed p-6">
        <!--begin::Icon-->
        <!--begin::Svg Icon | path: icons/duotone/Code/Warning-1-circle.svg-->
        <span class="svg-icon svg-icon-2tx svg-icon-success me-4">
          <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
            <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
            <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
          </svg>
        </span>
        <!--end::Svg Icon-->
        <!--end::Icon-->
        <!--begin::Wrapper-->
        <div class="d-flex flex-stack flex-grow-1">
          <!--begin::Content-->
          <div class="fw-bold">
            <h4 class="text-gray-800 fw-bolder">Congratulation!</h4>
            <div class="fs-6 text-gray-600">To start explore your account, please,
              <a href="<?php echo e(route('viewDashboard')); ?>" class="fw-bolder link-danger">go to DASHBOARD</a>
            </div>
          </div>
          <!--end::Content-->
        </div>
        <!--end::Wrapper-->
      </div>
      <!--end::Notice-->
      <!--end::Alert-->
    </div>
    <!--end::Body-->
  </div>
  <!--end::Wrapper-->
</div>
<!--end::Step 5--><?php /**PATH /home/devims1129cyou/public_html/resources/views/template/partials/getting-started/_step5.blade.php ENDPATH**/ ?>