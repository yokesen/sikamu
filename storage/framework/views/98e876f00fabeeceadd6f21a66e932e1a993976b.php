

<?php $__env->startSection('title','Getting Started'); ?>
<?php $__env->startSection('metadescription','Onboarding Client'); ?>
<?php $__env->startSection('metakeyword','Onboarding'); ?>
<?php $__env->startSection('bc-1','Dashboard'); ?>
<?php $__env->startSection('bc-2','Onboarding'); ?>

<?php $__env->startSection('container'); ?>

  <div id="kt_content_container" class="container">
    <?php echo $__env->make('app.partials._verifikasi-email', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/devims1129cyou/public_html/resources/views/app/ims-verifikasi-email.blade.php ENDPATH**/ ?>