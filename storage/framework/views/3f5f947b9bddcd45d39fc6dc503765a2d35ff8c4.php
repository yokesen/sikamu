
											<!--begin::Search-->
											<div id="kt_header_search" class="d-flex align-items-stretch" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="menu" data-kt-menu-trigger="auto" data-kt-menu-overflow="false" data-kt-menu-permanent="true" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
												<!--begin::Search toggle-->
												<div class="d-flex align-items-stretch" data-kt-search-element="toggle" id="kt_header_search_toggle">
													<div class="topbar-item px-3 px-lg-5">
														<i class="bi bi-search fs-3"></i>
													</div>
												</div>
												<!--end::Search toggle-->
												<!--begin::Menu-->
												<div data-kt-search-element="content" class="menu menu-sub menu-sub-dropdown p-7 w-325px w-md-375px">
													<!--begin::Wrapper-->
													<div data-kt-search-element="wrapper">

														<?php echo $__env->make('template.layout.search.partials._form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

														<?php echo $__env->make('template.layout.search.partials._results', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

														<?php echo $__env->make('template.layout.search.partials._main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

														<?php echo $__env->make('template.layout.search.partials._empty', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

													</div>
													<!--end::Wrapper-->

												<?php echo $__env->make('template.layout.search.partials._advanced-options', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

												<?php echo $__env->make('template.layout.search.partials._preferences', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

												</div>
												<!--end::Menu-->
											</div>
											<!--end::Search-->
<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/layout/search/_base.blade.php ENDPATH**/ ?>