
<!--begin::Menu wrapper-->
<div class="header-menu align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_header_menu_mobile_toggle" data-kt-place="true" data-kt-place-mode="prepend" data-kt-place-parent="{default: '#kt_body', lg: '#kt_header_nav'}">
	<!--begin::Menu-->
	<div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch" id="#kt_header_menu" data-kt-menu="true">
		<div class="menu-item me-lg-1">
			<a class="menu-link py-3" href="#">
				<span class="menu-title">Referrals</span>
			</a>
		</div>

		<div class="menu-item me-lg-1">
			<a class="menu-link py-3" href="#">
				<span class="menu-title">Deposit</span>
			</a>
		</div>

		<div class="menu-item me-lg-1">
			<a class="menu-link py-3" href="#">
				<span class="menu-title">Transfer</span>
			</a>
		</div>

		<div class="menu-item me-lg-1">
			<a class="menu-link py-3" href="#">
				<span class="menu-title">Withdraw</span>
			</a>
		</div>

	</div>
	<!--end::Menu-->
</div>
<!--end::Menu wrapper-->
<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/layout/header/_menu.blade.php ENDPATH**/ ?>