
							<!--begin::Container-->
							<div id="kt_content_container" class="container">

								<!--begin::Row-->
								<div class="row gy-5 g-xl-8">
									<!--begin::Col-->
									<div class="col-xxl-4">

										<?php echo $__env->make('template.partials.widgets.mixed._widget-2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xxl-4">

										<?php echo $__env->make('template.partials.widgets.mixed._widget-5', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xxl-4">

										<?php echo $__env->make('template.partials.widgets.mixed._widget-7', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

										<?php echo $__env->make('template.partials.widgets.mixed._widget-10', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row gy-5 gx-xl-8">
									<!--begin::Col-->
									<div class="col-xxl-4">

										<?php echo $__env->make('template.partials.widgets.lists._widget-3', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-8">

										<?php echo $__env->make('template.partials.widgets.tables._widget-9', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row gy-5 g-xl-8">
									<!--begin::Col-->
									<div class="col-xl-4">

										<?php echo $__env->make('template.partials.widgets.lists._widget-2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-4">

										<?php echo $__env->make('template.partials.widgets.lists._widget-6', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-4">

										<?php echo $__env->make('template.partials.widgets.lists._widget-4', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row g-5 gx-xxl-8">
									<!--begin::Col-->
									<div class="col-xxl-4">

										<?php echo $__env->make('template.partials.widgets.mixed._widget-5', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xxl-8">

										<?php echo $__env->make('template.partials.widgets.tables._widget-5', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->

							</div>
							<!--end::Container-->
<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/layout/content/_default.blade.php ENDPATH**/ ?>