

<?php $__env->startSection('title','Getting Started'); ?>
<?php $__env->startSection('metadescription','Onboarding Client'); ?>
<?php $__env->startSection('metakeyword','Onboarding'); ?>
<?php $__env->startSection('bc-1','Dashboard'); ?>
<?php $__env->startSection('bc-2','Onboarding'); ?>

<?php $__env->startSection('container'); ?>
  <!--begin::Container-->
  <div id="kt_content_container" class="container">
    <!--begin::Stepper-->
    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_create_account_stepper">
      <!--begin::Aside-->
      <div class="d-flex justify-content-center bg-white rounded justify-content-xl-start flex-row-auto w-100 w-xl-300px w-xxl-400px me-9 mb-6">
        <!--begin::Wrapper-->
        <div class="px-6 px-lg-10 px-xxl-15 py-20">
          <!--begin::Nav-->
          <div class="stepper-nav" id="nav-step">
            <?php echo $__env->make('pages.onboard.js.nav-step', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
          </div>
          <!--end::Nav-->
        </div>
        <!--end::Wrapper-->
      </div>
      <!--begin::Aside-->
      <!--begin::Content-->
      <div class="d-flex flex-row-fluid flex-center bg-white rounded" >
        <div class="py-20 w-100 w-xl-700px px-9" id="content-step">
          <?php echo $__env->make('pages.onboard.js.content-step-1', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
      </div>
      <!--end::Content-->
    </div>
    <!--end::Stepper-->
  </div>
  <!--end::Container-->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jsinline'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/devims1129cyou/public_html/resources/views/pages/onboard/master-getting-started.blade.php ENDPATH**/ ?>