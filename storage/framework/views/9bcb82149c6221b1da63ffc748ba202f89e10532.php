
								<!--begin::Page title-->
								<div data-kt-place="true" data-kt-place-mode="prepend" data-kt-place-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center me-3 flex-wrap mb-5 mb-lg-0 lh-1">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-gray fw-bolder my-1 fs-3"><?php echo $__env->yieldContent('bc-1'); ?>
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
									<!--end::Separator-->
									<!--begin::Description-->
									<small class="text-gray fs-7 fw-bold my-1 ms-1"><?php echo $__env->yieldContent('bc-2'); ?></small>
									<!--end::Description--></h1>
									<!--end::Title-->
								</div>
								<!--end::Page title-->
<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/layout/page-title/_default.blade.php ENDPATH**/ ?>