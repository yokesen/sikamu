										
										<!--begin::Mixed Widget 10-->
										<div class="card card-xxl-stretch-50 mb-5 mb-xl-8">
											<!--begin::Body-->
											<div class="card-body p-0 d-flex justify-content-between flex-column overflow-hidden">
												<div class="d-flex flex-stack flex-grow-1 px-9 pt-9 pb-3">
													<!--begin::Icon-->
													<div class="symbol symbol-45px">
														<div class="symbol-label">
															<!--begin::Svg Icon | path: icons/duotone/Shopping/Chart-line1.svg-->
															<span class="svg-icon svg-icon-2x svg-icon-primary">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z" fill="#000000" fill-rule="nonzero" />
																		<path d="M8.7295372,14.6839411 C8.35180695,15.0868534 7.71897114,15.1072675 7.31605887,14.7295372 C6.9131466,14.3518069 6.89273254,13.7189711 7.2704628,13.3160589 L11.0204628,9.31605887 C11.3857725,8.92639521 11.9928179,8.89260288 12.3991193,9.23931335 L15.358855,11.7649545 L19.2151172,6.88035571 C19.5573373,6.44687693 20.1861655,6.37289714 20.6196443,6.71511723 C21.0531231,7.05733733 21.1271029,7.68616551 20.7848828,8.11964429 L16.2848828,13.8196443 C15.9333973,14.2648593 15.2823707,14.3288915 14.8508807,13.9606866 L11.8268294,11.3801628 L8.7295372,14.6839411 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																	</g>
																</svg>
															</span>
															<!--end::Svg Icon-->
														</div>
													</div>
													<!--end::Icon-->
													<!--begin::Text-->
													<div class="d-flex flex-column text-end">
														<span class="fw-bolder text-gray-800 fs-3">Sales</span>
														<span class="text-gray-400 fw-bold">Oct 8 - Oct 26 21</span>
													</div>
													<!--end::Text-->
												</div>
												<!--begin::Chart-->
												<div class="mixed-widget-10-chart" data-kt-color="primary" style="height: 175px"></div>
												<!--end::Chart-->
											</div>
										</div>
										<!--end::Mixed Widget 10-->
										<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/partials/widgets/mixed/_widget-10.blade.php ENDPATH**/ ?>