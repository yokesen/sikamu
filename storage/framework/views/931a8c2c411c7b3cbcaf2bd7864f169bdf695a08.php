<!--begin::Stats-->
<div class="d-flex flex-wrap flex-stack">
  <?php if(Session::get('user')->id_cms_privileges > 7): ?>
    <!--begin::Wrapper-->
    <div class="d-flex flex-column flex-grow-1 pe-8">
      <!--begin::Stats-->
      <div class="d-flex flex-wrap">
        <!--begin::Stat-->
        <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
          <!--begin::Number-->
          <div class="d-flex align-items-center">
            <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-up.svg-->
            <span class="svg-icon svg-icon-3 svg-icon-success me-2">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <polygon points="0 0 24 0 24 24 0 24" />
                  <rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
                  <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
                </g>
              </svg>
            </span>
            <!--end::Svg Icon-->
            <div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="4500" data-kt-countup-prefix="$">0</div>
          </div>
          <!--end::Number-->
          <!--begin::Label-->
          <div class="fw-bold fs-6 text-gray-400">Deposit</div>
          <!--end::Label-->
        </div>
        <!--end::Stat-->
        <!--begin::Stat-->
        <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
          <!--begin::Number-->
          <div class="d-flex align-items-center">
            <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-down.svg-->
            <span class="svg-icon svg-icon-3 svg-icon-danger me-2">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <polygon points="0 0 24 0 24 24 0 24" />
                  <rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
                  <path d="M6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 C4.90236893,18.3165825 4.90236893,17.6834175 5.29289322,17.2928932 L11.2928932,11.2928932 C11.6714722,10.9143143 12.2810586,10.9010687 12.6757246,11.2628459 L18.6757246,16.7628459 C19.0828436,17.1360383 19.1103465,17.7686056 18.7371541,18.1757246 C18.3639617,18.5828436 17.7313944,18.6103465 17.3242754,18.2371541 L12.0300757,13.3841378 L6.70710678,18.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 14.999999) scale(1, -1) translate(-12.000003, -14.999999)" />
                </g>
              </svg>
            </span>
            <!--end::Svg Icon-->
            <div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="75">0</div>
          </div>
          <!--end::Number-->
          <!--begin::Label-->
          <div class="fw-bold fs-6 text-gray-400">Projects</div>
          <!--end::Label-->
        </div>
        <!--end::Stat-->
        <!--begin::Stat-->
        <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
          <!--begin::Number-->
          <div class="d-flex align-items-center">
            <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-up.svg-->
            <span class="svg-icon svg-icon-3 svg-icon-success me-2">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <polygon points="0 0 24 0 24 24 0 24" />
                  <rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
                  <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
                </g>
              </svg>
            </span>
            <!--end::Svg Icon-->
            <div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="60" data-kt-countup-prefix="%">0</div>
          </div>
          <!--end::Number-->
          <!--begin::Label-->
          <div class="fw-bold fs-6 text-gray-400">Success Rate</div>
          <!--end::Label-->
        </div>
        <!--end::Stat-->
      </div>
      <!--end::Stats-->
    </div>
    <!--end::Wrapper-->
  <?php endif; ?>

  

  <!--begin::Progress-->
  <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
    <div class="d-flex justify-content-between w-100 mt-auto mb-2">
      <span class="fw-bold fs-6 text-gray-400">Profile Completion</span>
      <span class="fw-bolder fs-6"><?php echo e(profileCompletion()); ?>%</span>
    </div>
    <div class="h-5px mx-3 w-100 bg-light mb-3">
      <div class="bg-warning rounded h-5px" role="progressbar" style="width: <?php echo e(profileCompletion()); ?>%;" aria-valuenow="<?php echo e(profileCompletion()); ?>" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  </div>
  <!--end::Progress-->
</div>
<!--end::Stats-->
<?php /**PATH /home/devims1129cyou/public_html/resources/views/app/partials/_statistic.blade.php ENDPATH**/ ?>