<div class="row">
  <div class="col-md-6">
    <!--begin::Basic info-->
    <div class="card mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Profil </h3>
        </div>
        <!--end::Card title-->
      </div>
      <!--begin::Card header-->
      <!--begin::Content-->
      <div id="kt_account_profile_details" class="collapse show">
        <!--begin::Form-->
        <form id="kt_account_profile_details_form" method="POST" action="<?php echo e(route('process-Edit-Profile')); ?>" class="form" enctype="multipart/form-data">
          <!--begin::Card body-->
          <div class="card-body border-top p-9">
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label fw-bold fs-6">Avatar</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8">
                <!--begin::Image input-->
                <div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url(assets/media/avatars/blank.png)">
                  <!--begin::Preview existing avatar-->
                  <div class="image-input-wrapper w-125px h-125px" style="background-image: url(<?php echo e(Session::get('user')->photo); ?>)"></div>
                  <!--end::Preview existing avatar-->
                  <!--begin::Label-->
                  <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                    <i class="bi bi-pencil-fill fs-7"></i>
                    <!--begin::Inputs-->
                    <input type="file" name="photo" max-size="3000" accept=".png, .jpg, .jpeg" />
                    <input type="hidden" name="avatar_remove" />
                    <!--end::Inputs-->
                  </label>
                  <!--end::Label-->
                  <!--begin::Cancel-->
                  <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                    <i class="bi bi-x fs-2"></i>
                  </span>
                  <!--end::Cancel-->
                  <!--begin::Remove-->
                  <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                    <i class="bi bi-x fs-2"></i>
                  </span>
                  <!--end::Remove-->
                </div>
                <!--end::Image input-->
                <!--begin::Hint-->
                <div class="form-text">Allowed file types: png, jpg, jpeg.</div>
                <!--end::Hint-->
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Username</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid <?php echo e(old('username') && !$errors->has('username') ? 'input-valid' : ''); ?> <?php echo e($errors->has('username') ? 'input-error' : ''); ?>" type="text" placeholder="" name="username" autocomplete="off" value="<?php echo e(old('username') ? old('username') : $user->username); ?>" required <?php echo e($errors->has('username') ? 'autofocus' : ''); ?>/>
                <?php if($errors->has('username')): ?>
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('name')); ?></h4>
                <?php endif; ?>
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid <?php echo e(old('name') && !$errors->has('name') ? 'input-valid' : ''); ?> <?php echo e($errors->has('name') ? 'input-error' : ''); ?>" type="text" placeholder="" name="name" autocomplete="off" value="<?php echo e(old('name') ? old('name') : $user->name); ?>" required <?php echo e($errors->has('name') ? 'autofocus' : ''); ?>/>
                <?php if($errors->has('name')): ?>
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('name')); ?></h4>
                <?php endif; ?>
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <?php if($user->email_verification == "new"): ?>
                  <input class="form-control form-control-lg form-control-solid <?php echo e(old('email') && !$errors->has('email') ? 'input-valid' : ''); ?> <?php echo e($errors->has('email') ? 'input-error' : ''); ?>" type="email" placeholder="" name="email" autocomplete="off" value="<?php echo e(old('email')); ?>" required <?php echo e($errors->has('email') ? 'autofocus' : ''); ?>/>
                  <?php if($errors->has('email')): ?>
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('email')); ?></h4>
                  <?php endif; ?>
                <?php else: ?>
                  <input class="form-control form-control-lg form-control-solid input-valid" type="email" name="email" value="<?php echo e($user->email); ?>" required readonly/>
                <?php endif; ?>

              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Whatsapp</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <?php if($user->whatsapp): ?>
                  <input class="form-control form-control-lg form-control-solid input-valid" value="<?php echo e($user->whatsapp); ?>" required readonly/>
                <?php elseif(empty($user->whatsapp)): ?>
                  <a href="https://wa.me/6281287608190/?text=verify <?php echo e($user->uuid); ?>" class="btn btn-success btn-lg" target="_blank"><i class="bi bi-whatsapp fs-2"></i> Verifikasi Whatsapp di sini</a>
                <?php endif; ?>

              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Tanggal Lahir</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid <?php echo e(old('dob') && !$errors->has('dob') ? 'input-valid' : ''); ?> <?php echo e($errors->has('dob') ? 'input-error' : ''); ?>" type="date"  placeholder="" name="dob" value="<?php echo e(old('dob') ? old('dob') : $user->dob); ?>" max="<?php echo e(date('Y-m-d', strtotime('- 17 years'))); ?>" autocomplete="off" required/>
                <?php if($errors->has('dob')): ?>
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> <?php echo e($errors->first('dob')); ?></h4>
                <?php endif; ?>
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->

            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Communication</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <!--begin::Options-->
                <div class="d-flex align-items-center mt-3">
                  <!--begin::Option-->
                  <label class="form-check form-check-inline form-check-solid me-5">
                    <input class="form-check-input" name="communication[]" type="checkbox" value="1" checked />
                    <span class="fw-bold ps-2 fs-6">Email</span>
                  </label>
                  <!--end::Option-->
                  <!--begin::Option-->
                  <label class="form-check form-check-inline form-check-solid">
                    <input class="form-check-input" name="communication[]" type="checkbox" value="2" checked />
                    <span class="fw-bold ps-2 fs-6">Whatsapp</span>
                  </label>
                  <!--end::Option-->
                </div>
                <!--end::Options-->
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-0">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label fw-bold fs-6">Allow Marketing</label>
              <!--begin::Label-->
              <!--begin::Label-->
              <div class="col-lg-8 d-flex align-items-center">
                <div class="form-check form-check-solid form-switch fv-row">
                  <input class="form-check-input w-45px h-30px" type="checkbox" name="allowmarketing" id="allowmarketing" checked="checked" />
                  <label class="form-check-label" for="allowmarketing"></label>
                </div>
              </div>
              <!--begin::Label-->
            </div>
            <!--end::Input group-->
          </div>
          <!--end::Card body-->
          <?php echo csrf_field(); ?>
          <!--begin::Actions-->
          <div class="card-footer d-flex justify-content-end py-6 px-9">
            <a href="<?php echo e(route('viewGettingStarted')); ?>" type="reset" class="btn btn-white btn-active-light-primary me-2">Discard</a>
            <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
          </div>
          <!--end::Actions-->
        </form>
        <!--end::Form-->
      </div>
      <!--end::Content-->
    </div>
  </div>
  <div class="col-md-6">

    <!--begin::Basic info-->
    <div class="card mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Dokumen KTP </h3>
        </div>
        <!--end::Card title-->
      </div>
      <!--begin::Card header-->
      <!--begin::Content-->
      <div class="collapse show">
        <!--begin::Form-->
        <form class="form" action="#" method="post">
          <!--begin::Card body-->
          <div class="card-body border-top p-9">

            <!--begin::Input group-->
            <div class="fv-row">
              <!--begin::Dropzone-->
              <div class="dropzone" id="ot_ktp">
                <!--begin::Message-->
                <div class="dz-message needsclick">
                  <!--begin::Icon-->
                  <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                  <!--end::Icon-->

                  <!--begin::Info-->
                  <div class="ms-4">
                    <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Klik di sini untuk upload.</h3>
                    <span class="fs-7 fw-bold text-gray-400">Upload foto KTP/SIM/Paspor</span>
                  </div>
                  <!--end::Info-->
                </div>
              </div>
              <!--end::Dropzone-->
            </div>
            <!--end::Input group-->
          </div>
          <!--end::Card body-->
        </form>
        <!--end::Form-->
      </div>
    </div>

  </div>
</div>
<?php /**PATH /home/devims1129cyou/public_html/resources/views/app/partials/_profil-edit.blade.php ENDPATH**/ ?>