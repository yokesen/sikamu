

<?php $__env->startSection('title','Getting Started'); ?>
<?php $__env->startSection('metadescription','Onboarding Client'); ?>
<?php $__env->startSection('metakeyword','Onboarding'); ?>
<?php $__env->startSection('bc-1','Dashboard'); ?>
<?php $__env->startSection('bc-2','Onboarding'); ?>

<?php $__env->startSection('container'); ?>

  <div id="kt_content_container" class="container">
    <?php echo $__env->make('app.partials._user-card', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


    <?php echo $__env->make('app.partials._bank-edit', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jsonpage'); ?>
  <script type="text/javascript">

  var myDropzone = new Dropzone("#ot_buku_rekening", {
      url: "<?php echo e(route('process-Upload-Tabungan')); ?>", // Set the url for your upload script location
      paramName: "gambar", // The name that will be used to transfer the file
      method: "post",
      headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
      maxFiles: 1,
      maxFilesize: 10, // MB
      addRemoveLinks: true,
      accept: function(file, done) {
        if (file.name == "wow.jpg") {
            done("Naha, you don't.");
        } else {
            done();
        }
      }
    });
  </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/devims1129cyou/public_html/resources/views/app/ims-bank.blade.php ENDPATH**/ ?>