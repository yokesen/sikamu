
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root" id="app">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">

				<?php echo $__env->make('template.layout.aside._base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">

					<?php echo $__env->make('template.layout.header._base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

						<?php echo $__env->make('template.layout.toolbars._toolbar-1', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">

							<?php echo $__env->yieldContent('container'); ?>

						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->

					<?php echo $__env->make('template.layout._footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		<!--begin::Drawers-->

		

		<!--begin::Modals-->

		<?php echo $__env->yieldContent('modals'); ?>

		<!--end::Modals-->
		<?php echo $__env->make('template.layout._scrolltop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<!--end::Main-->
<?php /**PATH /home/devims1129cyou/public_html/resources/views/template/layout/index.blade.php ENDPATH**/ ?>